﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.Enemy;
using System;
using System.Reflection;
using Control;
using Maths;
using Build.UI;
using static Maths.Utility;

namespace Actors.Tower
{
    public class Tower_Type2_Shield : TowerBase
    {
        float baseHealth = 4;
        float baseShield = 120;
        float baseLinearArmor = 0f;
        float baseChargeSpeed = 2f;

        [Header("Tower")]
        //public Sprite bulletSprite;
        //public Transform shotPos;
        //public float bulletSpeed = 30;
        public Tower_2_Shield actor = new Tower_2_Shield(null, "Barrier Tower",
            4,
            0.2f,
            0,
            6,
            1, 15);
        UIPropertySlider healthSlider, shieldSlider ,expSlider;
        List<TowerBase> towers = new List<TowerBase>(32);
        public override Actor Actor => actor;

        public override void Initilazie()
        {
            actor = new Tower_2_Shield(gameObject, actor);

            baseHealth = (float)actor.health.MaxValue;
            baseLinearArmor = actor.linearArmor;
            baseShield = (float)actor.shield.MaxValue;
            baseChargeSpeed = actor.shieldChargeSpeed;

            //Path = $"Defense/{Actor.name}";

            healthSlider = gameObject.AddComponent<UIPropertySlider>();
            healthSlider.SetData(actor, nameof(actor.health),1,true);
            expSlider = gameObject.AddComponent<UIPropertySlider>();
            expSlider.SetData(actor, nameof(actor.exp), 2);
            shieldSlider = gameObject.AddComponent<UIPropertySlider>();
            shieldSlider.SetData(actor, nameof(actor.shield), 0,true);

            base.Initilazie();

            visualModule.tracedTag = "Tower";
        }

        public override bool Select(object sender, EventArgs args)
        {
            if (base.Select(sender, args))
            {
                SelectedUIInfo.Instance.SelectActor(actor, sprite, (nameof(actor.shield), "Barrier", MorphsColors.Light_blue), (nameof(actor.health), "Health", MorphsColors.Light_green));
                return true;
            }
            return false;
        }

        public override void LvUp(object sender, EventArgs args)
        {
            base.LvUp(sender, args);
            int delta = (args as EventLevelArgs).deltaLv;

            Actor.health.MaxValue += baseHealth * delta * 0.058f;
            Actor.linearArmor += baseLinearArmor * delta * 0.31f;
            actor.shield.MaxValue += baseShield * delta * 0.075f;
            actor.shieldChargeSpeed += baseChargeSpeed * delta * 0.0375f;

            healthSlider.ForcedUpdate();
            expSlider.ForcedUpdate();
            shieldSlider.ForcedUpdate();
        }

        private void Start()
        {
            if (!initialized)
                Initilazie();
        }

        public override void Kill()
        {
            base.Kill();
            if (gameObject != null)
                Destroy(gameObject);
        }

        public override (PowerImageName imageName, MorphsColors imageColor, float value)[] GetUIData =>
            new (PowerImageName imageName, MorphsColors imageColor, float value)[] {
                (PowerImageName.Heart, MorphsColors.True_red, (float)Actor.health.MaxValue),
                (PowerImageName.Heart, MorphsColors.Blue, (float)actor.shield.MaxValue),
                (PowerImageName.Bow, MorphsColors.Green, (float)Actor.AttackModule.range),                
                (PowerImageName.Shield, MorphsColors.Yellow, (float)actor.linearArmorShield)
            };

        /* protected override void TriggerEnter(object sender, EventArgs args)
         {
             var collision = (args as EventTriggerArgs).collision;
             towers.Add(collision.GetComponent<TowerBase>());
             collision.GetComponent<TowerBase>().Actor.GetDamageHandler += GetTowerDamage;
         }
         protected override void TriggerExit(object sender, EventArgs args)
         {
             var collision = (args as EventTriggerArgs).collision;
             towers.Remove(collision.GetComponent<TowerBase>());
             collision.GetComponent<TowerBase>().Actor.GetDamageHandler -= GetTowerDamage;
         }*/
        int oldTowers = 0;
        private void Update()
        {
            if(actor.shieldDelay.Update(Time.deltaTime))
            {
                actor.shield.Value += actor.shieldChargeSpeed * Time.deltaTime;
            }
            actor.health.Value += 0.025f * Time.deltaTime;

            if (oldTowers != CurrentCountTowers)
            {
                towers.Clear();
                foreach (var col in visualModule.Cast())
                {
                    if (!col.gameObject.GetComponent<TowerBase>().ActorTag.Exist("Unprotected"))
                    {
                        col.gameObject.GetComponent<TowerBase>().Actor.GetDamageHandler += GetTowerDamage;
                        towers.Add(col.gameObject.GetComponent<TowerBase>());
                    }
                }
                oldTowers = CurrentCountTowers;
            }
        }

        void GetTowerDamage(object sender, ref DamageParams.EventDamageParams @params)
        {
            if (!actor.shield.IsMin)
            {
                @params.DamageRequest = false;
                (actor as IDamage).GetDamage(@params.Params.Owner, @params.Params);
            }
        }
       

    }

    [System.Serializable]
    public sealed class Tower_2_Shield : Actor, IDamage
    {
        public RangeValue shield = new RangeValue(0, 60, 1);
        public float linearArmorShield = 0;
        public float unlinearArmorShield = 0.2f;
        public float shieldChargeSpeed = 3.5f;
        public Timer shieldDelay = new Timer(5f);
        //((float damage, DamageParams.DamageType type) dmgParams, (float chanse, float mult) crit, float range, float attackPerMinute)
        public Tower_2_Shield(GameObject Root, string name, float health, float linearArmor, float unlinearArmor, float range ,int lv = 1, float baseEXP = 5) : base(Root, name, health, linearArmor, unlinearArmor, ((0, DamageParams.DamageType.Clear), (0, 0), range, 0), lv, baseEXP)
        {

        }
        public Tower_2_Shield(GameObject Root, Actor origin) : base(Root, origin)
        {

        }
        //public float regenPerSec = 0.1f;
        bool IDamage.GetDamage<T>(T damageOwner, DamageParams @params)
        {

            if (!IsAlive)
                return false;
            var adventage = GetAdvantageOfTheGround();
            if (shield.IsMin)
            {
                if (GetDamage(ref @params))
                {
                    float delta = (@params.GetTrueDamage(healthType) - (linearArmor + adventage.linear)) * (1 - (unlinearArmor + adventage.unlinear));
                    if (delta < DamageParams.minDamage)
                        delta = DamageParams.minDamage;
                    health.Value -= delta;
                    if (!IsAlive)
                    {
                        Root.GetComponent<TowerBase>().Kill();
                        return true;
                    }
                }
            }
            else
            {
                float delta = (@params.GetTrueDamage(healthType) - (linearArmorShield + 0)) * (1 - (unlinearArmor + 0));
                if (delta < DamageParams.minDamage)
                    delta = DamageParams.minDamage;
                shield.Value -= delta;
                AddEXP(delta / 4f);
                shieldDelay.Reset();
            }
            return false;
        }

        void IDamage.SetDamage<T>(T target, DamageParams @params)
        {
            /* var ITarget = target as IDamage;
             var dmg = new DamageParams(this, AttackModule.Damage, (AttackModule.Crit.chanse, AttackModule.Crit.mult), AttackModule.damageType);
             ITarget.GetDamage(this, dmg);*/
        }
    }
}

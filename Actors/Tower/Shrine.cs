﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.Enemy;
using System;
using System.Reflection;
using Control;
using Maths;

namespace Actors.Tower
{
    public class Shrine : TowerBase
    {
        float baseHealth = 10f;
        //float baseDmg = 0.6f;
        float baseLinearArmor = 0.5f;

        [Header("Tower")]
        //public Sprite bulletSprite;
        //public Transform shotPos;
        //public float bulletSpeed = 30;
        public Tower_Shrine actor = new Tower_Shrine(null, "Wooden Wall",
            300,
            4f,
            0,
            1, 15);
        UIPropertySlider healthSlider;
        //List<Actor> enemies = new List<Actor>(8);
        public override Actor Actor => actor;

        public override void Initilazie()
        {
            actor = new Tower_Shrine(gameObject, actor);

            baseHealth = (float)actor.health.MaxValue;
            baseLinearArmor = actor.linearArmor;

            //Path = $"Defense/{Actor.name}";

            healthSlider = gameObject.AddComponent<UIPropertySlider>();
            healthSlider.SetData(actor, nameof(actor.health),0,true);
            //expSlider = gameObject.AddComponent<UIPropertySlider>();
            //expSlider.SetData(actor, nameof(actor.exp), 1);

            ActorTag.AddTag("Unassembled");
            base.Initilazie();

            //visualModule.Range = 1.5f;
        }

        private void Start()
        {
            if (!initialized)
                Initilazie();
        }

        public override bool Select(object sender, EventArgs args)
        {
            if (base.Select(sender, args))
            {
                SelectedUIInfo.Instance.SelectActor(actor, sprite, (nameof(actor.health), "Health", Utility.MorphsColors.Light_green));
                return true;
            }
            return false;
        }

        public override void LvUp(object sender, EventArgs args)
        {
            base.LvUp(sender, args);
            int delta = (args as EventLevelArgs).deltaLv;

            Actor.health.MaxValue += baseHealth * delta * 0.058f;
            Actor.linearArmor += baseLinearArmor * delta * 0.31f;
            Actor.health.Value = Actor.health.MaxValue; //TODO (копия) заменить на эффект регенерации (когда появится поддержка эффектов)
            healthSlider.ForcedUpdate();
            //expSlider.ForcedUpdate();
        }

        public override void Kill()
        {
            base.Kill();
            Informator.SendMessage("Shrine is destroy, you lose, but game continue", Informator.AvalibleColors.Light_Red);
            if (gameObject != null)
                Destroy(gameObject);
        }
        float curentGold = 0;
        private void Update()
        {
            World.Instanse.material += actor.matPerSec * Time.deltaTime;
            if (curentGold < actor.getFullGold)
            {
                curentGold += actor.goldPerSec * Time.deltaTime;
            }
            else
            {
                World.Instanse.money += actor.getFullGold;
                curentGold -= actor.getFullGold;
            }
        }

        /*protected override void TriggerEnter(object sender, EventArgs args)
        {
            var collision = (args as EventTriggerArgs).collision;
            enemies.Add(collision.GetComponent<EnemyBase>().Actor);

        }
        protected override void TriggerExit(object sender, EventArgs args)
        {
            var collision = (args as EventTriggerArgs).collision;
            enemies.Remove(collision.GetComponent<EnemyBase>().Actor);
        }*/

        /* int oldEnemy = 0;
         private void Update()
         {
             if (oldEnemy != EnemyBase.CurrentCountEnemies)
             {

                 GetExp();
                 oldEnemy = EnemyBase.CurrentCountEnemies;
             }
         }

         void GetExp()
         {
             foreach (var act in enemies)
             {
                 if (!act.IsAlive)
                     Actor.AddEXP((float)act.exp.MaxValue / 4f);
             }
             enemies.RemoveAll(x => !x.IsAlive);
         }*/

    }

    [System.Serializable]
    public class Tower_Shrine : Actor, IDamage
    {
        public float matPerSec = 0.05f, goldPerSec = 0.5f;
        public int getFullGold = 5;
        //((float damage, DamageParams.DamageType type) dmgParams, (float chanse, float mult) crit, float range, float attackPerMinute)
        public Tower_Shrine(GameObject Root, string name, float health, float linearArmor, float unlinearArmor, int lv = 1, float baseEXP = 5) : base(Root, name, health, linearArmor, unlinearArmor, ((0, DamageParams.DamageType.Clear), (0, 0), 0, 0), lv, baseEXP)
        {

        }
        public Tower_Shrine(GameObject Root, Actor origin) : base(Root, origin)
        {

        }
        //public float regenPerSec = 0.1f;
        bool IDamage.GetDamage<T>(T damageOwner, DamageParams @params)
        {

            if (!IsAlive)
                return false;
            if (GetDamage(ref @params))
            {
                var adventage = GetAdvantageOfTheGround();
                float delta = (@params.GetTrueDamage(healthType) - (linearArmor + adventage.linear)) * (1 - (unlinearArmor + adventage.unlinear));
                if (delta < DamageParams.minDamage)
                    delta = DamageParams.minDamage;
                health.Value -= delta;
                Informator.SendMessage("Shrine is under attack!", Informator.AvalibleColors.Light_Yellow);
                if (!IsAlive)
                {
                    Root.GetComponent<TowerBase>().Kill();
                    return true;
                }
            }
            return false;
        }

        void IDamage.SetDamage<T>(T target, DamageParams @params)
        {
            /* var ITarget = target as IDamage;
             var dmg = new DamageParams(this, AttackModule.Damage, (AttackModule.Crit.chanse, AttackModule.Crit.mult), AttackModule.damageType);
             ITarget.GetDamage(this, dmg);*/
        }
    }
}


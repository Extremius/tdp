﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
//Temporary
using TMPro;

public class World : MonoBehaviour
{
    public static World Instanse;
    public static Vector3 DeltaPosition = new Vector3(0, 0, -950);
    public Grid grid;
    public Tilemap tilemap;
    public int money = 50;
    public float material= 20, materialPerSec = 0f;
    //Temporary
    public TextMeshProUGUI mn,mt;
    private void Awake()
    {
        if (Instanse == null)
            Instanse = this;
        else if (Instanse != this)
            Destroy(gameObject);

        grid = GetComponentInChildren<Grid>();
        tilemap = GetComponentInChildren<Tilemap>();
    }
    //TODO: Создать класс Player включающий в себя все необходимые свойства и методы для инструментов
    private void Update()
    {
        mn.text = $"{money}";
        material += materialPerSec * Time.deltaTime;
        mt.text = $"{material :0.0}";
    }
}

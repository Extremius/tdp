﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;

public class CameraMove : MonoBehaviour
{
    const float deltaTimeSet = 0.15f; 
    public float sensetive =15;
    [Header("Ortographic")]
    public float scrollSensetive = 15f;
    private RangeValue size = new RangeValue(3, 8, 0.2f);
    [Header("Game zone")]
    public Rect bounds;
    public Transform posXY, posHW;
    Vector3 position;
    Timer deltaTime = new Timer(deltaTimeSet);

    private void Start()
    {
        Input.simulateMouseWithTouches = true;
        posHW.transform.parent = posXY;
        bounds = new Rect(posXY.position, posHW.localPosition);
        
    }
    private void Update()
    {

/*#if UNITY_ANDROID
        if (Input.touchCount == 1)
        {
            if (deltaTime.Update(Time.deltaTime))
            {
                transform.Translate(-Input.touches[0].deltaPosition * (1f / Screen.dpi) * Time.deltaTime * sensetive);
            }
        }
        else
        {
            deltaTime.Reset();
        }
#endif*/

        if (Input.GetMouseButton(0))
        {
            if (deltaTime.Update(Time.deltaTime))
            {
                //Debug.Log("Pressed " + Input.mouseScrollDelta.ToString());
                Vector3 delta = -(Input.mousePosition - position) * (1f / Screen.dpi) * Time.deltaTime * sensetive * ((size.Percent/100f)+0.2f);
               
                if(bounds.Contains(transform.position + delta))
                    transform.Translate(delta);
                
            }
        }
        else
        {
            deltaTime.Reset();
        }

        if(Input.mouseScrollDelta != Vector2.zero)
        {
            size.Value += -Input.mouseScrollDelta.y * scrollSensetive * Time.deltaTime;
            Camera.main.orthographicSize = (float)size.Value;
        }
        position = Input.mousePosition;

        if(Input.touchCount >=2)
        {
            if(!scrolling)
            {
                baseDistance = Vector2.Distance(Input.touches[0].position,Input.touches[1].position);
               
                scrolling = true;
            }
            else
            {
                deltaDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position) - baseDistance;
                size.Value += -deltaDistance * scrollSensetive * Time.deltaTime / Screen.dpi;
                deltaDistance = Vector2.Distance(Input.touches[0].position, Input.touches[1].position);
                Camera.main.orthographicSize = (float)size.Value;
            }
        }
        else
        {
            scrolling = false;
        }

    }
    float deltaDistance;
    float baseDistance;
    bool scrolling = false;
   
}


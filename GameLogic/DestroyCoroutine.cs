﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;

public class DestroyCoroutine : MonoBehaviour
{
    Timer delay;
    bool inited = false;
    public void InitDestroy(float timeDelay)
    {
        if (inited)
            return;
        inited = true;
        delay = new Timer(timeDelay);
        StartCoroutine(DestroyRoutine());
    }

    private IEnumerator DestroyRoutine()
    {
        while (!delay.Update(Time.deltaTime))
            yield return new WaitForEndOfFrame();
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.Enemy;
using System;
using Control;
using Build.UI;
using static Maths.Utility;

namespace Actors.Tower
{
    public sealed class Tower_Type0 : TowerBase
    {
        float baseHealth = 10f;
        float baseDmg = 0.6f;
        float baseLinearArmor = 0.5f;

        [Header("Tower")]
        public Sprite bulletSprite;
        public Transform shotPos;
        public float bulletSpeed = 30;
        public Tower_0 actor = new Tower_0(null, "Basic Tower",
            10,
            0.5f,
            0,
            ((0.6f,DamageParams.DamageType.Physics),
            (10f,1.5f), //crit
            3.5f, //range
            120f), // attack/min
            1, 15);
        UIPropertySlider healthSlider, expSlider;
        List<EnemyBase> targets = new List<EnemyBase>(9);

        public override Actor Actor => actor;

        public override void Initilazie()
        {
            actor = new Tower_0(gameObject, actor);

            baseHealth = (float)actor.health.MaxValue;
            baseDmg = actor.AttackModule.Damage;
            baseLinearArmor = actor.linearArmor;

            //Path = $"Offense/{Actor.name}";

            healthSlider = gameObject.AddComponent<UIPropertySlider>();
            healthSlider.SetData(actor, nameof(actor.health), 0, true);
            expSlider = gameObject.AddComponent<UIPropertySlider>();
            expSlider.SetData(actor, nameof(actor.exp), 1, false);
            attackDelayTimer = new Maths.Timer(60f / actor.AttackModule.AttacksPerMinute);

            base.Initilazie();
        }

        private void Start()
        {
            if(!initialized)
                Initilazie();
        }

        public override bool Select(object sender, EventArgs args)
        {
            if(base.Select(sender, args))
            {
                SelectedUIInfo.Instance.SelectActor(actor, sprite, (nameof(actor.health), "Health", MorphsColors.Light_green));
                return true;
            }
            return false;
        }
        protected override void TriggerEnter(object sender, EventArgs args)
        {
            var collision = (args as EventTriggerArgs).collision;
            if (collision.tag == "Enemy")
                targets.Add(collision.GetComponent<EnemyBase>());
        }
        protected override void TriggerExit(object sender, EventArgs args)
        {
            var collision = (args as EventTriggerArgs).collision;
            if (collision.tag == "Enemy")
                targets.Remove(collision.GetComponent<EnemyBase>());
        }

        public override void LvUp(object sender, EventArgs args)
        {
            base.LvUp(sender, args);
            int delta = (args as EventLevelArgs).deltaLv;

            Actor.health.MaxValue += baseHealth * delta * 0.15f;
            Actor.AttackModule.Damage += baseDmg * delta * 0.25f;
            Actor.linearArmor += baseLinearArmor * delta * 0.35f;
            healthSlider.ForcedUpdate();
            expSlider.ForcedUpdate();
        }

        public override void Kill()
        {
            base.Kill();
            if (gameObject != null)
                Destroy(gameObject);
        }

        private void Update()
        {
            Shot();
            Regen();
        }
        Vector2 dir;
        bool Shot()
        {
            if (targets.Count <= 0)
                return false;
            if(attackDelayTimer.Update(Time.deltaTime))
            {
                attackDelayTimer.Reset(60f / actor.AttackModule.AttacksPerMinute);

                if (targets[0] == null)
                {
                    var tmp = targets.ToArray();
                    targets.Clear();
                    for(int i =0;i<tmp.Length;i++)
                    {
                        if (tmp[i] != null)
                            targets.Add(tmp[i]);   
                    }
                }
                if (targets.Count <= 0)
                    return false;
                Vector2 enemyNextPosition = (Vector2)targets[0].transform.position + targets[0].Direction * targets[0].speed * (Vector2.Distance(targets[0].transform.position, shotPos.position) / bulletSpeed);
                dir = enemyNextPosition - (Vector2)shotPos.position; dir.Normalize();
                TowerBullet.InstanceBullet(this, bulletSprite, "Enemy", 1, (dir, bulletSpeed)).transform.position = shotPos.position;

                return true;
            }
            return false;
        }

        void Regen()
        {
            Actor.health.Value += actor.regenPerSec * Time.deltaTime;
           
        }
    }

    [System.Serializable]
    public class Tower_0 : Actor, IDamage
    {
        public Tower_0(GameObject Root, string name, float health, float linearArmor, float unlinearArmor, ((float damage, DamageParams.DamageType type) dmgParams, (float chanse, float mult)crit, float range, float attackPerMinute) attackModule, int lv = 1, float baseEXP = 5) : base(Root, name, health, linearArmor, unlinearArmor, attackModule, lv, baseEXP)
        {

        }
        public Tower_0(GameObject Root, Actor origin) : base (Root, origin)
        {

        }
        public float regenPerSec = 0.1f;
        bool IDamage.GetDamage<T>(T damageOwner, DamageParams @params)
        {
            
            if (!IsAlive)
                return false;
            if (GetDamage(ref @params))
            {
                var adventage = GetAdvantageOfTheGround();
                float delta = (@params.GetTrueDamage(healthType) - (linearArmor + adventage.linear)) * (1 - (unlinearArmor + adventage.unlinear));
                if (delta < DamageParams.minDamage)
                    delta = DamageParams.minDamage;
                health.Value -= delta;
                if (!IsAlive)
                {
                    Root.GetComponent<TowerBase>().Kill();
                    return true;
                }
            }
            return false;
        }

        void IDamage.SetDamage<T>(T target, DamageParams @params)
        {
            var ITarget = target as IDamage;
            var dmg = new DamageParams(this, AttackModule.Damage, (AttackModule.Crit.chanse, AttackModule.Crit.mult), AttackModule.damageType);
            ITarget.GetDamage(this, dmg);
        }
    }
}

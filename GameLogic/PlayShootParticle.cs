﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Particles
{
    public enum ParticleType
    {
        Explosion_1,
    }
    public static class PlayShootParticle
    {
        

        public static void Create(Vector3 position, ParticleType particleType)
        {
            //var go = new GameObject("Particle").AddComponent<PlayShootParticle>();


            var go = GameObject.Instantiate(Resources.Load<ParticleSystem>($"Prefabs/Particles/{particleType}"));
            go.transform.position = position;
            go.Play();
            go.gameObject.AddComponent<DestroyCoroutine>().InitDestroy(go.main.duration);

        }
    }
}

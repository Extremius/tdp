﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.Tower;
using System;
using Control;
using Build.UI;

namespace Build
{
    public class BuildManager : MonoBehaviour
    {
        public static BuildManager Instance;
        [Header("Colors")]
        public Color[] colors = new Color[2];
        [Header("Avalible Builds")]
        public List<TowerBase> buildList;
        public List<Builder> builders;
        public BuilderManagerUI UI;
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

           
        }

        private void Start()
        {
            Selectable.Instance.OnDoubleClick += BuildObject;
            for(int i=0;i<3;i++)
                Builder.InstanceNewBuilder().gameObject.SetActive(false);
        }

        //TODO: Сделать вызов панели со списком возможных построек при срабатывании ивента OnDoubleClick

        public void BuildObject(object sender, EventArgs args)
        {
            Vector2 pos = (args as EventSelectArgs).OnWorldSelectedPosition;
            Selectable.Instance.UnSelectable = true;
            UI.Call(pos);
            
        }

        public static void ApplyToBuild(int buildId, Vector2 pos)
        {
            //OLD VERSION 0.3
            if (Instance.buildList[buildId].GetBuildData.buildPrice <= World.Instanse.money && Instance.buildList[buildId].GetBuildData.buildMaterialPrice <= World.Instanse.material)
            {
                for (int i = 0; i < Instance.builders.Count; i++)
                {

                    var res = Instance.builders[i].StartBuild(buildId, Instance.buildList[buildId].buildTime, pos);
                    switch (res.msg)
                    {
                        case Builder.BuilderOutputs.OK:
                            Debug.Log("Start Building");
                            World.Instanse.money -= Instance.buildList[buildId].GetBuildData.buildPrice;
                            World.Instanse.material -= Instance.buildList[buildId].GetBuildData.buildMaterialPrice;
                            Selectable.Instance.UnSelectable = true;
                            return;
                        case Builder.BuilderOutputs.Builder_is_busy:
                            //Informator.SendMessage("All builders is busy, plesae wait", Informator.AvalibleColors.Light_Yellow);
                            break;
                        case Builder.BuilderOutputs.Build_overlapping:
                            Informator.SendMessage("Current buildig overlapping, can't build", Informator.AvalibleColors.Light_Red);
                            //Debug.Log("Overlapping");
                            return;
                    }

                }
            }
            else
            {
                 if(Instance.buildList[buildId].GetBuildData.buildPrice > World.Instanse.money && Instance.buildList[buildId].GetBuildData.buildMaterialPrice > World.Instanse.material)
                    Informator.SendMessage("Not enought money and materials", Informator.AvalibleColors.Light_Red);

               else if (Instance.buildList[buildId].GetBuildData.buildPrice > World.Instanse.money)
                    Informator.SendMessage("Not enought money", Informator.AvalibleColors.Light_Red);
                
               else if(Instance.buildList[buildId].GetBuildData.buildMaterialPrice > World.Instanse.material)
                    Informator.SendMessage("Not enought materials", Informator.AvalibleColors.Light_Red);
                
            }
            
        }

        public static void DisassambleTower(TowerBase tower)
        {
            for (int i = 0; i < Instance.builders.Count; i++)
            {
               var res = Instance.builders[i].Disassamble(tower);
                switch(res.msg)
                {
                    case Builder.BuilderOutputs.Builder_is_busy:
                        continue;
                    case Builder.BuilderOutputs.Builder_cant_disassambled:
                        Informator.SendMessage("Can't disassembled this building");
                        return;
                    case Builder.BuilderOutputs.OK:
                        return;
                }
            }
        }

        public static int FreeBuilders
        {
            get
            {
                int count = 0;
                foreach(var bld in Instance.builders)
                {
                    if (!bld.Busy)
                        count++;
                }

                return count;
            }
        }

    }

    interface IBuilding
    {
        (int buildPrice, float buildMaterialPrice ,float buildTime) GetBuildData { get; }
        (PowerImageName imageName, Maths.Utility.MorphsColors imageColor, float value)[] GetUIData { get; }
    }
}

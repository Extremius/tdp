﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UnityEngine.Tilemaps
{
    public enum GroundType
    {
        NULL = 0,
        River = 10,

        Road = 1,
        Plain,
        Hill,
        Forest,
        Mountain,
    }

    public class GroundTile : Tile
    {
        public GroundType groundType;      
        /*public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
        {
            base.GetTileData(position, tilemap, ref tileData);

           /* if (tileData.gameObject == null)
            {
                tileData.gameObject = new GameObject(tileData.sprite.name);
            }
            obj = tileData.gameObject.AddComponent<GroundTileObj>();
            obj.SetTileBase(this);
            obj.transform.position = position;
           

            tileData.colliderType = colliderType;
            tileData.sprite = sprite;
            tileData.color = color;
        }*/
        


    }

#if UNITY_EDITOR
    [CustomEditor(typeof(GroundTile))]
    public class GroundTileEditor : Editor
    {
        private GroundTile tile { get => target as GroundTile; }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            /*if (tile.sprite != null)
                EditorGUI.DrawPreviewTexture(new Rect(10, 75, 40, 40), tile.sprite.texture);
            if(tile.sprite==null)
                tile.sprite = (Sprite)EditorGUILayout.ObjectField("null",tile.sprite, typeof(Sprite), true);
            else
                tile.sprite = (Sprite)EditorGUILayout.ObjectField(tile.sprite.name, tile.sprite, typeof(Sprite), true);
            tile.color = EditorGUILayout.ColorField(tile.color);
            tile.colliderType = (Tile.ColliderType)EditorGUILayout.EnumPopup(tile.colliderType);
            tile.groundType = (GroundType)EditorGUILayout.EnumPopup(tile.groundType);*/
        }

        

    }
#endif
}

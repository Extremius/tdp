﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Maths;

public class PhysUPS_DEBUG : MonoBehaviour
{
    public TextMeshProUGUI text;
    Timer fpsDelay = new Timer(0.5f);
    float delta; int count;

    private void Update()
    {
        if (fpsDelay.Update(Time.deltaTime))
        {
            text.text = $"FPS { delta/count:0}";
            fpsDelay.Reset();
            delta = 0; count = 0;
        }
        else
        {
            delta += 1f/Time.deltaTime;
            count++;
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;
using UnityEngine.Tilemaps;

namespace Actors
{
    interface IDamage
    {
        /// <summary>
        /// Получает урон
        /// </summary>
        /// <param name="damageOwner">Владелец урона</param>
        /// <param name="params">Данные о уроне</param>
        /// <returns></returns>
        bool GetDamage<T>(T damageOwner, DamageParams @params) where T : Actor;
        /// <summary>
        /// Наносит урон
        /// </summary>
        /// <param name="target">Цель урона</param>
        /// <param name="params">Данные о уроне</param>
        void SetDamage<T>(T target, DamageParams @params) where T : Actor;
    }

    public struct DamageParams
    {
        public const float minDamage = 0.2f;
        public enum DamageType
        {
            Clear,
            Physics,
            Magic,
        }
        public Actor Owner { get; }
        float baseDmg;
        DamageType damageType;
        public float GetTrueDamage(Actor.HealthType healthType)
        {
            switch (healthType)
            {
                case Actor.HealthType.Flesh:
                    switch (damageType)
                    {
                        case DamageType.Clear:
                            return baseDmg * 1;
                        case DamageType.Physics:
                            return baseDmg * 1.5f;
                        case DamageType.Magic:
                            return baseDmg * 0.75f;
                    }
                    break;
                case Actor.HealthType.Rock:
                    switch (damageType)
                    {
                        case DamageType.Clear:
                            return baseDmg * 1;
                        case DamageType.Physics:
                            return baseDmg * 0.5f;
                        case DamageType.Magic:
                            return baseDmg * 1.25f;
                    }
                    break;
                case Actor.HealthType.Fur:
                    switch (damageType)
                    {
                        case DamageType.Clear:
                            return baseDmg * 1;
                        case DamageType.Physics:
                            return baseDmg * 0.75f;
                        case DamageType.Magic:
                            return baseDmg * 1.5f;
                    }
                    break;
                case Actor.HealthType.Shell:
                    switch (damageType)
                    {
                        case DamageType.Clear:
                            return baseDmg * 1;
                        case DamageType.Physics:
                            return baseDmg * 1.25f;
                        case DamageType.Magic:
                            return baseDmg * 0.5f;
                    }
                    break;
            }
            return 1;
        }

        public DamageParams(Actor owner, float baseDmg, (float chanse, float mult) crit, DamageType dmgType)
        {
            Owner = owner;
            if (CryptoRandom.Chanse100(crit.chanse))
                baseDmg *= crit.mult;
            //Rounded
            float deltaRounded = CryptoRandom.NextFloat(0.9f, 1.1f);
            this.baseDmg = baseDmg * deltaRounded;
            damageType = dmgType;
        }
        public class EventDamageParams
        {
            private DamageParams param;
            public ref DamageParams Params { get => ref param; }
            private bool damageRequest = true;
            public bool DamageRequest { get => damageRequest; set
                {
                    if (value == false)
                        damageRequest = false;
                }
            }
            public EventDamageParams(ref DamageParams damage)
            {
                param = damage;
            }
        }
        public delegate void DamageHandler(object sender, ref EventDamageParams @params);
    }

   


    [System.Serializable]
    public class Actor
    {
        public enum HealthType
        {
            Flesh,
            Rock,
            Fur,
            Shell,
        }
        public event System.EventHandler LevelChangeHandler;
        public readonly GameObject Root;
        public string name = "Unknow Actor";
        public int lv = 1;
        public RangeValue exp = new RangeValue(0, 5, 0);
        public RangeValue health = new RangeValue(0, 10, 1);
        public HealthType healthType;
        public GroundType groundType;
        //Events true если урон прошел false если не прошел
        public event DamageParams.DamageHandler GetDamageHandler;
        /// <summary>
        /// Вызывает ивент который обрабатывается на подписчиках
        /// </summary>
        /// <param name="params"></param>
        /// <returns></returns>
        public bool GetDamage(ref DamageParams @params)
        {
            var arg = new DamageParams.EventDamageParams(ref @params);
            GetDamageHandler?.Invoke(this, ref arg);
            return arg.DamageRequest;
        }
        public event DamageParams.DamageHandler SetDamageHandler;
        //Events end
        /// <summary>
        /// Единичная защита от повреждений
        /// </summary>
        public float linearArmor = 0; //В единицах
        /// <summary>
        /// Процентная защита
        /// </summary>
        public float unlinearArmor  //% 0..1
        {
            get => _unlinearArmor;
            set
            {
                if (value < 0)
                    _unlinearArmor = 0;
                else if (value > 1)
                    _unlinearArmor = 1;
                else
                    _unlinearArmor = value;
            }
        }
        [SerializeField]
        protected float _unlinearArmor = 0;

        [SerializeField]
        private Actor_AttackModule attackModule;
        public Actor_AttackModule AttackModule { get => attackModule; }

        public bool IsAlive
        {
            get => !health.IsMin;
        }
        public bool AddEXP(float deltaExp)
        {
            float dlt; int oldLv = lv;
            while(deltaExp >0)
            {
                dlt = (float)(exp.MaxValue - exp.Value);
                if (deltaExp >= dlt)
                {
                    exp.Value = exp.MinValue; exp.MaxValue *= 1.15f;
                    deltaExp -= dlt; lv++;
                }
                else
                {
                    exp.Value += deltaExp;
                    break;
                }
            }
            if (oldLv != lv)
            {
                LevelChangeHandler?.Invoke(this, new EventLevelArgs() { deltaLv = lv-oldLv});
                return true;
            }
            else return false;

        }

        /// <summary>
        /// Возвращает кортеж данных, где
        /// linear : дополнительная защита
        /// unlinear : дополнительная нелинейная защита
        /// range : дополнительная дальность атаки
        /// speed : модификатор скорости
        /// </summary>
        /// <returns></returns>
        public (float linear, float unlinear, float range, float speed) GetAdvantageOfTheGround()
        {
            switch (groundType)
            {
                case GroundType.NULL:
                    return (0, 0, 0, 1);
                case GroundType.River:
                    return (-2, -0.4f, -1.5f, 0.4f);
                case GroundType.Road:
                    return (-1, 0, 0, 1.35f);
                case GroundType.Plain:
                    return (1, 0, -0.5f, 0.9f);
                case GroundType.Hill:
                    return (2, 0, 1f, 0.7f);
                case GroundType.Forest:
                    return (3, 0.2f, -2f, 0.6f);
                case GroundType.Mountain:
                    return (4, 0.4f, 2f, 0.2f);

                default:
                    return (0, 0, 0, 1);
            }
            
        }
        public void GetCurrentGround(Vector3 pos)
        {
            pos -= Vector3.one * 0.5f;
            pos.z = World.Instanse.transform.position.z;
           var tmp = World.Instanse.tilemap.GetTile<GroundTile>(new Vector3Int((int)pos.x, (int)pos.y, (int)World.Instanse.grid.transform.position.z))?.groundType;
            if (tmp != null)
                groundType = (GroundType)tmp;
        }

        public Actor(GameObject Root, string name, float health, float linearArmor, float unlinearArmor,((float damage, DamageParams.DamageType type) dmgParams, (float chanse, float mult) crit, float range, float attackPerMinute) attackModule, int lv = 1,float baseEXP = 5)
        {
            this.Root = Root;
            this.name = name;
            this.health = new RangeValue(0, health, 1);
            this.linearArmor = linearArmor;
            this.unlinearArmor = unlinearArmor;
            this.lv = lv;
            this.exp = new RangeValue(0, baseEXP, 0);
            this.attackModule = new Actor_AttackModule(this, attackModule);
        }
        public Actor(GameObject Root, Actor origin)
        {
            this.Root = Root;
            name = origin.name;
            health = origin.health;
            linearArmor = origin.linearArmor;
            unlinearArmor = origin.unlinearArmor;
            lv = origin.lv;
            exp = origin.exp;
            attackModule = origin.AttackModule;
        }
    }

    [System.Serializable]
    public class Actor_AttackModule
    {
        public readonly Actor Root;
        [SerializeField]
        private float damage;
        public float Damage
        {
            
            get => damage;
            set
            {
                if (value < 0)
                    damage = 0;
                else
                    damage = value;
            }
        }
        
        public (float chanse, float mult) Crit;
        public DamageParams.DamageType damageType;
        public float range;
        [SerializeField]
        float apm;
        public float AttacksPerMinute
        {
            get => apm;
            set
            {
                if (value < 0)
                    apm = 0;
                else
                    apm = value;
            }
        }

        public Actor_AttackModule(Actor root,((float damage, DamageParams.DamageType type) dmgParams, (float chanse, float mult)crit,float range, float apm) data)
        {
            Root = root;
            this.damage = data.dmgParams.damage;
            damageType = data.dmgParams.type;
            Crit = data.crit;
            this.range = data.range;
            this.apm = data.apm;
        }
    }

    class EventLevelArgs : System.EventArgs
    {
        public int deltaLv;
    }
}

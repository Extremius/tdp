﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Control
{
    public class SelectorHelpUI : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            if (Selectable.Instance.UnSelectable && !Selectable.Instance.Dragging)
            {
                Selectable.Instance.UnSelectable = false;
                Selectable.Instance.DeselectAll();
            }
        }
    }
}

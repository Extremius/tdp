﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GroundTileObj : MonoBehaviour
{
    

    Tile tilebase;
    public GroundType groundType = GroundType.Plain;
    public void SetTileBase<T>(T tilebase) where T:Tile
    {
        this.tilebase = tilebase as Tile;
    }

    public Tile Tilebase { get => tilebase; }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Maths;
using Actors.Enemy;

public class EnemySpawn : MonoBehaviour
{
    public Transform position;
    public List<EnemyBase> prefabs;
    [Header("Spawn delay")]
    public float spawnDelay = 5;
    public float minSpawnDelay = 2;
    public float deltaSpawnDelay = 0;
    [Header("Level params")]
    public float timeToNextLevel = 30;
    public int minLevel = -1, maxLevel = 3, baseLevel = 2;
    

    Timer _nextLevel;
    Timer _spawnDelay = new Timer(5);
    float spawnChanse = 0.5f;

    private void Start()
    {
        _nextLevel = new Timer(timeToNextLevel);
    }
    GameObject go; int id = 0;
    int deltaLv = 0;
    private void Update()
    {
        if(_spawnDelay.Update(Time.deltaTime))
        {
            if(CryptoRandom.Chanse1(spawnChanse))
            {
                id = CryptoRandom.NextInt32(0, prefabs.Count);
                while (prefabs[id] == null)
                    Next(ref id);
                go = Instantiate(prefabs[id].gameObject);
                go.transform.position = position.position;
                deltaLv = CryptoRandom.NextInt32(minLevel, maxLevel + 1);
                if (deltaLv < 1)
                    deltaLv = 1;
                go.GetComponent<EnemyBase>().Initialize();
                go.GetComponent<EnemyBase>().LevelSet(baseLevel + deltaLv );
                
                spawnDelay += deltaSpawnDelay;
                if (spawnDelay < minSpawnDelay)
                    spawnDelay = minSpawnDelay;
                _spawnDelay.Reset(spawnDelay);
            }
        }

        if(_nextLevel.Update(Time.deltaTime))
        {
            _nextLevel.Reset(timeToNextLevel);
            baseLevel++;
        }
    }

    private void Next(ref int id)
    {
        id++;
        if (id >= prefabs.Count)
            id = 0;
    }

}

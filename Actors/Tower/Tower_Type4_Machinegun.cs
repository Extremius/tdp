﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.Enemy;
using System;
using Control;
using Build.UI;
using Maths;
using static Maths.Utility;

namespace Actors.Tower
{
    public sealed class Tower_Type4_Machinegun : TowerBase
    {
        float baseHealth = 10f;
        float baseDmg = 0.6f;
        float baseLinearArmor = 0.5f;

        [Header("Tower")]
        public Sprite bulletSprite;
        public Transform shotPos;
        public float bulletSpeed = 30;
        public Tower_4_Machinegun actor = new Tower_4_Machinegun(null, "Machinegun",
            6,
            0.4f,
            0,
            ((0.32f, DamageParams.DamageType.Physics),
            (2f, 1.5f), //crit
            2.5f, //range
            240), // attack/min
            1, 15);
        UIPropertySlider healthSlider, ammoSlider ,expSlider;
        List<EnemyBase> targets = new List<EnemyBase>(9);

        public override Actor Actor => actor;

        public override void Initilazie()
        {
            actor = new Tower_4_Machinegun(gameObject, actor);

            baseHealth = (float)actor.health.MaxValue;
            baseDmg = actor.AttackModule.Damage;
            baseLinearArmor = actor.linearArmor;

            //Path = $"Offense/{Actor.name}";

            healthSlider = gameObject.AddComponent<UIPropertySlider>();
            healthSlider.SetData(actor, nameof(actor.health), 0, true);
            ammoSlider = gameObject.AddComponent<UIPropertySlider>();
            ammoSlider.SetData(actor, nameof(actor.ammo), 1, false);
            expSlider = gameObject.AddComponent<UIPropertySlider>();
            expSlider.SetData(actor, nameof(actor.exp), 2, false);
            attackDelayTimer = new Maths.Timer(60f / actor.AttackModule.AttacksPerMinute);

            base.Initilazie();

           
        }

        private void Start()
        {
            if (!initialized)
                Initilazie();
        }

        public override bool Select(object sender, EventArgs args)
        {
            if (base.Select(sender, args))
            {
                SelectedUIInfo.Instance.SelectActor(actor, sprite, (nameof(actor.health), "Health", MorphsColors.Light_green), (nameof(actor.ammo), "Ammo", MorphsColors.Dark_yellow));
                return true;
            }
            return false;
        }
        protected override void TriggerEnter(object sender, EventArgs args)
        {
            var collision = (args as EventTriggerArgs).collision;
            if (collision.tag == "Enemy")
                targets.Add(collision.GetComponent<EnemyBase>());
        }
        protected override void TriggerExit(object sender, EventArgs args)
        {
            var collision = (args as EventTriggerArgs).collision;
            if (collision.tag == "Enemy")
                targets.Remove(collision.GetComponent<EnemyBase>());
        }

        public override void LvUp(object sender, EventArgs args)
        {
            base.LvUp(sender, args);
            int delta = (args as EventLevelArgs).deltaLv;

            Actor.health.MaxValue += baseHealth * delta * 0.15f;
            Actor.AttackModule.Damage += baseDmg * delta * 0.17f;
            Actor.linearArmor += baseLinearArmor * delta * 0.29f;
            healthSlider.ForcedUpdate();
            expSlider.ForcedUpdate();
        }

        public override void Kill()
        {
            base.Kill();
            if (gameObject != null)
                Destroy(gameObject);
        }

        /*public override (PowerImageName imageName, MorphsColors imageColor, float value)[] GetUIData =>
           new (PowerImageName imageName, MorphsColors imageColor, float value)[] {
                (PowerImageName.Heart, MorphsColors.True_red, (float)Actor.health.MaxValue),
                (PowerImageName.Fist, MorphsColors.Red, (float)Actor.AttackModule.Damage),
                (PowerImageName.Bow, MorphsColors.Green, (float)Actor.AttackModule.range),
                (PowerImageName.Lightning, MorphsColors.Blue, (float)Actor.AttackModule.AttacksPerMinute),
                (PowerImageName.Shield, MorphsColors.Yellow, (float)Actor.linearArmor)
           };*/

        private void Update()
        {
            Shot();
            Regen();
            Reload();
        }

        Timer deltaForReload = new Timer(6f);
        void Reload()
        {
            if (actor.ammo.IsMax)
                return;

            if(deltaForReload.Update(Time.deltaTime) || actor.ammo.IsMin)
                if(actor.reloadDelay.Update(Time.deltaTime))
                {
                    actor.ammo.Value = actor.ammo.MaxValue;
                    actor.reloadDelay.Reset();
                    deltaForReload.Reset();
                }
        }

        Vector2 dir;
        bool Shot()
        {
            if (targets.Count <= 0)
                return false;

            if (attackDelayTimer.Update(Time.deltaTime))
            {
                if (!actor.ammo.IsMin)
                {
                    actor.ammo.Value -= 1;
                    actor.reloadDelay.Reset();
                    deltaForReload.Reset();
                    attackDelayTimer.Reset(60f / actor.AttackModule.AttacksPerMinute);

                    if (targets[0] == null)
                    {
                        var tmp = targets.ToArray();
                        targets.Clear();
                        for (int i = 0; i < tmp.Length; i++)
                        {
                            if (tmp[i] != null)
                                targets.Add(tmp[i]);
                        }
                    }
                    if (targets.Count <= 0)
                        return false;
                    Vector2 enemyNextPosition = (Vector2)targets[0].transform.position + targets[0].Direction * targets[0].speed * (Vector2.Distance(targets[0].transform.position, shotPos.position) / bulletSpeed);
                    dir = enemyNextPosition - (Vector2)shotPos.position; dir.Normalize();
                    TowerBullet.InstanceBullet(this, bulletSprite, "Enemy", 1, (dir, bulletSpeed)).transform.position = shotPos.position;

                    return true;
                }
                /*else if (actor.reloadDelay.Update(Time.deltaTime))
                {
                    actor.ammo.Value = actor.ammo.MaxValue;
                    actor.reloadDelay.Reset();
                    deltaForReload.Reset();
                }*/
            }
            return false;
        }

        void Regen()
        {
            Actor.health.Value += actor.regenPerSec * Time.deltaTime;

        }
    }

    [System.Serializable]
    public class Tower_4_Machinegun : Actor, IDamage
    {
        public RangeValue ammo = new RangeValue(0, 45, 1);
        public Timer reloadDelay = new Timer(8f);
        public Tower_4_Machinegun(GameObject Root, string name, float health, float linearArmor, float unlinearArmor, ((float damage, DamageParams.DamageType type) dmgParams, (float chanse, float mult) crit, float range, float attackPerMinute) attackModule, int lv = 1, float baseEXP = 5) : base(Root, name, health, linearArmor, unlinearArmor, attackModule, lv, baseEXP)
        {

        }
        public Tower_4_Machinegun(GameObject Root, Actor origin) : base(Root, origin)
        {

        }
        public float regenPerSec = 0.1f;
        bool IDamage.GetDamage<T>(T damageOwner, DamageParams @params)
        {

            if (!IsAlive)
                return false;
            if (GetDamage(ref @params))
            {
                var adventage = GetAdvantageOfTheGround();
                float delta = (@params.GetTrueDamage(healthType) - (linearArmor + adventage.linear)) * (1 - (unlinearArmor + adventage.unlinear));
                if (delta < DamageParams.minDamage)
                    delta = DamageParams.minDamage;
                health.Value -= delta;
                if (!IsAlive)
                {
                    Root.GetComponent<TowerBase>().Kill();
                    return true;
                }
            }
            return false;
        }

        void IDamage.SetDamage<T>(T target, DamageParams @params)
        {
            var ITarget = target as IDamage;
            var dmg = new DamageParams(this, AttackModule.Damage, (AttackModule.Crit.chanse, AttackModule.Crit.mult), AttackModule.damageType);
            ITarget.GetDamage(this, dmg);
        }
    }
}
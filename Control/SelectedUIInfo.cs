﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Actors;
using Maths;
using System.Reflection;
using static Maths.Utility;

namespace Control
{
    public class SelectedUIInfo : MonoBehaviour
    {
        [SerializeField, Header("Main")]
        private Actor selectedActor;
        private Sprite spr;
        public static SelectedUIInfo Instance;

        [Header("Graphics")]
        public Image spriteActor;
        public TextMeshProUGUI textActorName,
            textPower0Value,
            textPower1Value,
            textPower2Value,
            textPower3Value,
            textLevel,
            textSlider0,
            textSlider1,
            textSlider2,
            textSlider0Value,
            textSlider1Value,
            textSlider2Value,
            textArtifact0,
            textArtifact1,
            textGround;
        public Slider sliderLevel,
            slider0,
            slider1,
            slider2,
            backgroundSlider0,
            backgroundSlider1,
            backgroundSlider2;

        //private
        private bool slider0Enable = false, slider1Enable = false, slider2Enable = false;
        private RangeValue sliderValue0, sliderValue1, sliderValue2, deltaExp,
            deltaValue0, deltaValue1, deltaValue2,
            deltaLocalValue0, deltaLocalValue1, deltaLocalValue2;
        private const float startTime = 0.7f;
        private Timer timer0 = new Timer(startTime), timer1 = new Timer(startTime), timer2 = new Timer(startTime);
        private float avalibleDelta = 0.001f;
        private FieldInfo field0, field1, field2;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);
        }
        private void Start()
        {
            slider0.minValue = slider1.minValue = slider2.minValue = backgroundSlider0.minValue = backgroundSlider1.minValue = backgroundSlider2.minValue = sliderLevel.minValue = 0;
            slider0.maxValue = slider1.maxValue = slider2.maxValue = backgroundSlider0.maxValue = backgroundSlider1.maxValue = backgroundSlider2.maxValue = sliderLevel.maxValue = 100;

            DeselectActor();
            textArtifact0.text = "";
            textArtifact1.text = "";

            GetComponentInChildren<UIButton>().OnPressButtonEvent += DisasambleCurentTower;
            GetComponentInChildren<UIButton>().Interactble = true;
        }

        public void DisasambleCurentTower(object sender, System.EventArgs args)
        {
            Build.BuildManager.DisassambleTower(selectedActor.Root.GetComponent<Actors.Tower.TowerBase>());
            DeselectActor();
            Selectable.Instance.UnSelectable = false;
            Selectable.Instance.selectGraphic.gameObject.SetActive(false);
        }

        public void SelectActor(Actor select, Sprite img, params (string systemName, string simpleName, MorphsColors color)[] sliders)
        {
            selectedActor = select;

            gameObject.SetActive(true);
            spriteActor.sprite = img;
            textActorName.text = selectedActor.name;
            textLevel.text = selectedActor.lv.ToString();
            textPower0Value.text = Utility.MorphSingle(selectedActor.AttackModule.Damage);
            float value = selectedActor.AttackModule.range + selectedActor.GetAdvantageOfTheGround().range;
            if (value < 0)
                value = 0;
            textPower1Value.text = Utility.MorphSingle(value);
            textPower2Value.text = Utility.MorphSingle(selectedActor.AttackModule.AttacksPerMinute);
            value = selectedActor.linearArmor + selectedActor.GetAdvantageOfTheGround().linear;
            textPower3Value.text = Utility.MorphSingle(value);
            textGround.text = $"Ground: {selectedActor.groundType}";

            deltaExp = selectedActor.exp;
            sliderLevel.value = deltaExp.Percent;
            oldLv = selectedActor.lv;

            if (sliders != null)
            {
                switch (sliders.Length)
                {
                    case 1:
                        slider0Enable = true;
                        break;
                    case 2:
                        slider0Enable = true;
                        slider1Enable = true;
                        break;
                    case 3:
                        slider0Enable = true;
                        slider1Enable = true;
                        slider2Enable = true;
                        break;
                }
                Color color;
                if (slider0Enable)
                {
                    field0 = selectedActor.GetType().GetField(sliders[0].systemName);
                    sliderValue0 = (RangeValue)field0.GetValue(selectedActor);
                    deltaValue0 = deltaLocalValue0 = sliderValue0;
                    textSlider0.text = sliders[0].simpleName;
                    textSlider0Value.text = $"{sliderValue0.Value:0} / {sliderValue0.MaxValue: 0}";

                    slider0.value = backgroundSlider0.value = sliderValue0.Percent;
                    slider0.transform.Find("Fill").GetComponent<Image>().color = MorphColor(sliders[0].color);
                    color = MorphColor(sliders[0].color) * 0.5f; color.a = 1;
                    textSlider0Value.color = color;


                    slider0.gameObject.SetActive(true);
                }
                if (slider1Enable)
                {
                    field1 = selectedActor.GetType().GetField(sliders[1].systemName);
                    sliderValue1 = (RangeValue)field1.GetValue(selectedActor);
                    deltaValue1 = deltaLocalValue1 = sliderValue1;
                    textSlider1.text = sliders[1].simpleName;
                    textSlider1Value.text = $"{sliderValue1.Value:0} / {sliderValue1.MaxValue: 0}";

                    slider1.value = backgroundSlider1.value = sliderValue1.Percent;
                    slider1.transform.Find("Fill").GetComponent<Image>().color = MorphColor(sliders[1].color);
                    color = MorphColor(sliders[1].color) * 0.5f; color.a = 1;
                    textSlider1Value.color = color;

                    slider1.gameObject.SetActive(true);
                }
                if (slider2Enable)
                {
                    field2 = selectedActor.GetType().GetField(sliders[2].systemName);
                    sliderValue2 = (RangeValue)field2.GetValue(selectedActor);
                    deltaValue2 = deltaLocalValue2 = sliderValue2;
                    textSlider2.text = sliders[2].simpleName;
                    textSlider2Value.text = $"{sliderValue2.Value:0} / {sliderValue2.MaxValue: 0}";

                    slider2.value = backgroundSlider2.value = sliderValue2.Percent;
                    slider2.transform.Find("Fill").GetComponent<Image>().color = MorphColor(sliders[2].color);
                    color = MorphColor(sliders[2].color) * 0.5f; color.a = 1;
                    textSlider2Value.color = color;


                    slider2.gameObject.SetActive(true);
                }

            }
        }

        public void DeselectActor(bool force = false)
        {
            if (selectedActor == null && !force)
                return;
            slider0.gameObject.SetActive(false);
            slider1.gameObject.SetActive(false);
            slider2.gameObject.SetActive(false);
            slider0Enable = slider1Enable = slider2Enable = false;
            timer0.Reset(); timer1.Reset(); timer2.Reset();
            backgroundSlider0.value = backgroundSlider1.value = backgroundSlider2.value = 0;
            selectedActor = null;
            gameObject.SetActive(false);
        }

        /*private Color GetColor(AvalibleColors color)
        {
            switch (color)
            {
                case AvalibleColors.Light_Green:
                    return new Color(0.3632075f, 1f, 0.4422306f, 1f);
                case AvalibleColors.Light_Yellow:
                    return new Color(0.9903116f, 1f, 0.3647059f, 1f);
                case AvalibleColors.Light_Blue:
                    return new Color(0.3647059f, 0.5435221f, 1f, 1f);

                default:
                    return Color.red;
            }
        }*/

        public void Reset()
        {
            if (selectedActor == null)
            {
                DeselectActor(true);
                return;
            }

            textLevel.text = selectedActor.lv.ToString();
            textPower0Value.text = selectedActor.AttackModule.Damage.ToString("0.0");
            textPower1Value.text = (selectedActor.AttackModule.range + selectedActor.GetAdvantageOfTheGround().range).ToString("0.0");
            textPower2Value.text = selectedActor.AttackModule.AttacksPerMinute.ToString();
            textPower3Value.text = (selectedActor.linearArmor + selectedActor.GetAdvantageOfTheGround().linear).ToString("0.0");

            textGround.text = selectedActor.groundType.ToString();

            deltaExp = selectedActor.exp;
            sliderLevel.value = deltaExp.Percent;
            oldLv = selectedActor.lv;

            if (slider0Enable)
                sliderValue0 = deltaValue0 = deltaLocalValue0 = (RangeValue)field0.GetValue(selectedActor);
            if (slider1Enable)
                sliderValue1 = deltaValue1 = deltaLocalValue1 = (RangeValue)field1.GetValue(selectedActor);
            if (slider2Enable)
                sliderValue2 = deltaValue2 = deltaLocalValue2 = (RangeValue)field2.GetValue(selectedActor);
        }

        private float delta;
        private readonly float k = 8;
        private int oldLv;

        private void Update()
        {
            if (selectedActor == null)
            {
                DeselectActor(true);
            }

            //exp
            delta = (float)(selectedActor.exp.Value - deltaExp.Value);
            if (Mathf.Abs(delta) >= avalibleDelta)
            {
                deltaExp.Value += delta * Time.deltaTime * k;
                sliderLevel.value = deltaExp.Percent;
            }
            //sliders
            //slider 0
            if (slider0Enable)
            {
                sliderValue0 = (RangeValue)field0.GetValue(selectedActor);
                delta = (float)(sliderValue0.Value - deltaValue0.Value);
                if (Mathf.Abs(delta) >= avalibleDelta)
                {
                    deltaValue0.Value += delta * Time.deltaTime * k;
                    slider0.value = deltaValue0.Percent;
                    textSlider0Value.text = $"{sliderValue0.Value:0} / {sliderValue0.MaxValue: 0}";

                    if (delta < 0)
                        timer0.Reset();
                }

                if (timer0.Update(Time.deltaTime))
                {
                    delta = (float)(sliderValue0.Value - deltaLocalValue0.Value);
                    if (Mathf.Abs(delta) >= avalibleDelta)
                    {
                        deltaLocalValue0.Value += delta * Time.deltaTime * k;
                        //Debug.Log($"Min {deltaLocalValue0.MinValue} Value {deltaLocalValue0.Value} Max {deltaLocalValue0.MaxValue} Percent {deltaLocalValue0.Percent}");
                        backgroundSlider0.value = deltaLocalValue0.Percent;
                    }
                }
            }

            //slider 1
            if (slider1Enable)
            {
                sliderValue1 = (RangeValue)field1.GetValue(selectedActor);
                delta = (float)(sliderValue1.Value - deltaValue1.Value);
                if (Mathf.Abs(delta) >= avalibleDelta)
                {
                    deltaValue1.Value += delta * Time.deltaTime * k;
                    slider1.value = deltaValue1.Percent;
                    textSlider1Value.text = $"{sliderValue1.Value:0} / {sliderValue1.MaxValue: 0}";

                    if (delta < 0)
                        timer1.Reset();
                }

                if (timer1.Update(Time.deltaTime))
                {
                    delta = (float)(sliderValue1.Value - deltaLocalValue1.Value);
                    if (Mathf.Abs(delta) >= avalibleDelta)
                    {
                        deltaLocalValue1.Value += delta * Time.deltaTime * k;
                        backgroundSlider1.value = deltaLocalValue1.Percent;
                    }
                }
            }

            //slider 2
            if (slider2Enable)
            {
                sliderValue2 = (RangeValue)field2.GetValue(selectedActor);
                delta = (float)(sliderValue2.Value - deltaValue2.Value);
                if (Mathf.Abs(delta) >= avalibleDelta)
                {
                    deltaValue2.Value += delta * Time.deltaTime * k;
                    slider2.value = deltaValue2.Percent;
                    textSlider2Value.text = $"{sliderValue2.Value:0} / {sliderValue2.MaxValue: 0}";

                    if (delta < 0)
                        timer2.Reset();
                }

                if (timer2.Update(Time.deltaTime))
                {
                    delta = (float)(sliderValue2.Value - deltaLocalValue2.Value);
                    if (Mathf.Abs(delta) >= avalibleDelta)
                    {
                        deltaLocalValue2.Value += delta * Time.deltaTime * k;
                        backgroundSlider2.value = deltaLocalValue2.Percent;
                    }
                }
            }

            //Check reset
            if (selectedActor.lv != oldLv)
                Reset();
        }

    }
}

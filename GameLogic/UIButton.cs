﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Maths;
using System;

public sealed class UIButton : MonoBehaviour, IPointerDownHandler, IPointerExitHandler ,IPointerUpHandler, IPointerClickHandler
{
    

   
    public static UIButton Selected { get; private set; }
    Timer deltaTime = new Timer(0.5f);
    /// <summary>
    /// Срабатывает после некоторого времени зажатия
    /// </summary>
    public event EventHandler OnPressButtonEvent;
    /// <summary>
    /// Срабатывает после клика
    /// </summary>
    public event EventHandler OnClickButtonEvent;
    public bool changeImg = false;
    private bool _interactble;
    public bool Interactble
    {
        get => _interactble;

        set
        {
            if(_interactble!=value)
            {
                var img = GetComponent<Image>();
                if (img!=null && changeImg)
                {
                    Color color = img.color;
                    if (value)
                    {
                        color *= 2f; color.a = 1;
                    }
                    else
                    {
                        color *= 0.5f; color.a = 1;
                    }
                    img.color = color;
                }
                _interactble = value;
            }
        }
        
    }
    [SerializeField]
    Image tiledImage;
    Coroutine thisCoroutine;
    void Start()
    {
        if(tiledImage == null)
        {
            tiledImage.GetComponent<Image>();
        }
    }
    
    IEnumerator UpdateFrame ()
    {
        while (true)
        {
           
            if (deltaTime.Update(Time.deltaTime))
            {
                
                OnPressButtonEvent?.Invoke(this, null);
                Deselect();
            }
            else if (deltaTime.countDown)
            {
                if (tiledImage != null)
                {
                    
                    tiledImage.fillAmount = 1f - deltaTime.Percent;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!Interactble)
            return;
        deltaTime.Reset();
        Selected = this;
        if (tiledImage != null)
        {
            tiledImage.fillAmount = 0f;
        }

       thisCoroutine = StartCoroutine(UpdateFrame());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
       
        Deselect();
    }
    public void OnPointerExit(PointerEventData eventData)
    {
       
        Deselect();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (deltaTime.Percent <= 0.1f)
            return;
        OnClickButtonEvent?.Invoke(this,null);
    }

    void Deselect()
    {
        if (thisCoroutine != null)
        {
            StopCoroutine(thisCoroutine);
            thisCoroutine = null;
        }
        deltaTime.Reset();
        deltaTime.countDown = false;
        if (tiledImage != null)
        {
            tiledImage.fillAmount = 0;
        }
        if (Selected == this)
            Selected = null;
    }

   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointsControl : MonoBehaviour
{
    public static WayPointsControl Instance;
    public bool autoDepth = true;
    public int maxDepth = 2;
    public bool autoBranchs = true;
    public int maxBranchs = 2;
    private Dictionary<int, Transform> keyPoints;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(this);

        if (Instance != this)
            return;
        keyPoints = new Dictionary<int, Transform>(transform.childCount);
        Transform point;
        if (autoBranchs)
            maxBranchs = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            point = transform.GetChild(i);

            keyPoints.Add(GetFirstIntFromName(point.name), point);
            if (autoBranchs)
            {
                foreach(var chr in point.name)
                {
                    if (chr == '/')
                        maxBranchs++;
                }
            }
            if(autoDepth)
            {
                int l = 1;
                foreach (var chr in point.name)
                {
                    if (chr == '/')
                        l++;
                }
                if (maxDepth < l)
                    maxDepth = l;
            }
        }
        

    }
    public Transform[] BuildWay(params int[] depth)
    {
        if (depth == null)
            depth = new int[] { 1 };
        else
            for (int i = 0; i < depth.Length; i++)
                if (depth[i] < 1)
                    depth[i] = 1;


        List<Transform> way = new List<Transform>(transform.childCount);
        Transform point;
        way.Add(keyPoints[0]); int depthKey = 0;
        for(int i =0;i<keyPoints.Count;)
        {
            /* point = transform.GetChild(i);
             if(!way.Contains(keyPoints[GetFirstIntFromName(point.name)]))
                 way.Add(keyPoints[GetFirstIntFromName(point.name)]);
             if (!way.Contains(keyPoints[GetSecondIntFromName(point.name, depth)]))
                 way.Add(keyPoints[GetSecondIntFromName(point.name, depth)]);*/

            
            i = GetSecondIntFromName(keyPoints[i].name, ref depthKey ,depth[depthKey]);

            if (i == -1)
                return way.ToArray();

            //Debug.Log(depthKey);
            if (depthKey == depth.Length)
            {
                depthKey = 0;
            }
            point = keyPoints[i];
            if (!way.Contains(point))
            {
                way.Add(point);
            }

        }

        return way.ToArray();
    }

    private int GetFirstIntFromName(string name)
    {
        
        int i = 0, n = 0;
        while (!char.IsNumber(name[n]))
            n++;
        while (name[i] != '-')
            i++;
        return int.Parse(name.Substring(n, i - n));
    }
    private int GetSecondIntFromName(string name, ref int depthKey ,int depth =1)
    {

        int i = 0;

        while (name[i] != '-')
            i++;
        i++;

        int j = i;
        while (depth > 0 && j<name.Length)
        {
            while (j<name.Length && name[j] != '/')
                j++;
            depth--;
            
            if (depth > 0 && j < name.Length)
            {
                j++;
                i = j;
                
                //Debug.Log("NEXT");
            }
        }

        int grade = 0;
        for (int l = 0; l < name.Length; l++)
            if (name[l] == '/')
                if (++grade == 2)
                    break;
        if (grade > 1)
            depthKey++;

        string format = name.Substring(i, j - i);
        if (format != "F")
            return int.Parse(format);
        else return -1;
    }

    
}

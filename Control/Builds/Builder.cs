﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;
using UnityEngine.UI;
using TMPro;
using Actors.Tower;

namespace Build
{
    public class Builder : MonoBehaviour
    {
        public enum BuilderOutputs
        {
            OK,
            Builder_is_busy,
            Build_overlapping,
            Builder_cant_disassambled,
        }
        enum OperationType
        {
            Building,
            Idle,
            Disassamble,
        }

        private Image progressBar;
        private TextMeshProUGUI percentText, nameText;
        private BoxCollider2D box2D;
        private float timeToBuid = 0;
        private int buildId = -1;

        private OperationType operation = OperationType.Idle;
        [SerializeField]
        Timer buildTime = new Timer(1);
        public float buildPower = 1;
        public bool Busy { get; private set; }
        public float Progress { get => 1f - buildTime.Percent; }

        public (BuilderOutputs msg, bool result) StartBuild(int index, float time, Vector2 worldPosition)
        {
            if (Busy)
                return (BuilderOutputs.Builder_is_busy, false);
            int x = (int)(worldPosition.x / Mathf.Abs(worldPosition.x)),
                y = (int)(worldPosition.y / Mathf.Abs(worldPosition.y));
            worldPosition = new Vector2((int)worldPosition.x, (int)worldPosition.y);
            worldPosition += new Vector2(x,y) * 0.5f;
            transform.position = worldPosition;

            /*foreach (var bld in BuildManager.Instance.builders)
                if (bld != this && bld.Busy && Vector2.Distance(bld.transform.position, worldPosition) <= 0.1f)
                    return (BuilderOutputs.Build_overlapping, false);*/
            var cols = Physics2D.OverlapBoxAll(worldPosition + BuildManager.Instance.buildList[index].GetComponent<BoxCollider2D>().offset, BuildManager.Instance.buildList[index].GetComponent<BoxCollider2D>().size * 0.9f, 0);
            if (cols != null)
            {
                foreach (var col in cols)
                {
                    if(col.tag == "Tower" || col.tag == "Enemy" || col.tag == "Builder")
                        return (BuilderOutputs.Build_overlapping, false);
                }
            }
            buildTime.Reset(time/buildPower);
            Busy = true;
            buildId = index;
            gameObject.SetActive(true);

            nameText.text = BuildManager.Instance.buildList[buildId].Actor.name;
            progressBar.fillAmount = 0;
            percentText.text = $"0%";
            timeToBuid = time / buildPower;

            box2D.offset = BuildManager.Instance.buildList[buildId].GetComponent<BoxCollider2D>().offset;
            box2D.size = BuildManager.Instance.buildList[buildId].GetComponent<BoxCollider2D>().size;
            progressBar.color = BuildManager.Instance.colors[0];
            operation = OperationType.Building;

            return (BuilderOutputs.OK, true);
        }

        public void BuildComplete()
        {
            buildTime.countDown = false;
            var go = Instantiate(BuildManager.Instance.buildList[buildId]);
            go.transform.position = transform.position;
            Busy = false;
            operation = OperationType.Idle;
            Informator.SendMessage("Build complete", Informator.AvalibleColors.Light_Green);
            box2D.offset = Vector3.zero;
            box2D.size = Vector3.zero;
            go.GetComponent<Actors.Tower.TowerBase>().Initilazie();
            gameObject.SetActive(false);
        }

        public static Builder InstanceNewBuilder()
        {
            var go = new GameObject("Builder", typeof(Builder), typeof(SpriteRenderer));
            var component = go.GetComponent<Builder>();
            component.Busy = false;
            BuildManager.Instance.builders.Add(component);
            go.transform.SetParent(BuildManager.Instance.transform);
            var spr = go.GetComponent<SpriteRenderer>();
            spr.sprite = Resources.Load<Sprite>("Sprites/Circle");
            spr.color = new Color(0.6792453f, 0.6792453f, 0.6792453f, 0.75f);

            var goUi = Instantiate(Resources.Load<GameObject>("Prefabs/BuilderUI"));
            goUi.transform.SetParent(go.transform, false);
            goUi.transform.localPosition = Vector3.zero;

            go.AddComponent<Rigidbody2D>().isKinematic = true;
            go.tag = "Builder";

            component.box2D = go.AddComponent<BoxCollider2D>();
            component.box2D.isTrigger = true;
            component.box2D.offset = Vector3.zero;
            component.box2D.size = Vector3.zero;

            component.progressBar = goUi.transform.Find("ProgresBar").GetComponent<Image>();
            component.percentText = goUi.transform.Find("Percent").GetComponent<TextMeshProUGUI>();
            component.nameText = goUi.transform.Find("Name").GetComponent<TextMeshProUGUI>();

            return component;
        }

        public (BuilderOutputs msg, bool result) Disassamble(TowerBase target)
        {
            if (Busy)
                return (BuilderOutputs.Builder_is_busy, false);
            if(target.ActorTag.Exist("Unassembled"))
                return (BuilderOutputs.Builder_cant_disassambled, false);
            var data = target.GetBuildData;
            buildTime.Reset((data.buildTime*0.5f) / buildPower);
            operation = OperationType.Disassamble;
            disassambleTowerRef = target;
            transform.position = target.transform.position;
            target.gameObject.SetActive(false);
            Busy = true;
            progressBar.color = BuildManager.Instance.colors[1];
            progressBar.fillAmount = 0;
            percentText.text = $"0%";
            timeToBuid = (data.buildTime*0.5f) / buildPower;
            nameText.text = "Disassembling";
            gameObject.SetActive(true);
            return (BuilderOutputs.OK, true);
        }

        private void CompleteDisassamble()
        {
            Busy = false;
            operation = OperationType.Idle;
            int getGold = (int)((disassambleTowerRef.GetBuildData.buildPrice * CryptoRandom.NextFloat(0.3f,0.6f)) * (disassambleTowerRef.Actor.health.Percent/100f));
            float getMaterial = (disassambleTowerRef.GetBuildData.buildMaterialPrice * CryptoRandom.NextFloat(0.3f, 0.6f)) *( disassambleTowerRef.Actor.health.Percent / 100f);
            Informator.SendMessage($"Disassemble compelte\nGold: {getGold} Material: {getMaterial : 0.0}", Informator.AvalibleColors.Light_Green);
            World.Instanse.money += getGold; World.Instanse.material += getMaterial;
            disassambleTowerRef.Kill();
            gameObject.SetActive(false);
        }

        private TowerBase disassambleTowerRef;

        bool updateUI = true;
        
        private void Update()
        {
            if (!Busy)
                return;
            if(buildTime.Update(Time.deltaTime))
            {
                switch(operation)
                {
                    case OperationType.Building:
                        BuildComplete();
                        break;
                    case OperationType.Disassamble:
                        CompleteDisassamble();
                        break;
                }
            }
            if(updateUI)
            {
                progressBar.fillAmount = Progress;
                if (timeToBuid > 120)
                    percentText.text = $"{Progress * 100f: 0.0}%";
                else
                    percentText.text = $"{Progress * 100f: 0}%";
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;
using System;
using TMPro;

namespace Actors.Enemy
{
    public abstract class EnemyBase : ActorBase
    {
        /*
        public Enemy_0 actor = new Enemy_0(null,"Goblin",1,0,5);*/

        public static int CurrentCountEnemies { get; protected set; }

       
        [Header("Main")]
        public float speed = 1.5f;
        [SerializeField]
        protected Transform[] way;
        [Header("Render")]
        
        protected Timer delayFromAdventages = new Timer(0.5f);

        //Hidden
        protected VisualModule closeVisualModule;

        public virtual void Initialize()
        {
            Actor.GetCurrentGround(transform.position);
            //actor = new Enemy_0(gameObject, actor.name, actor.lv, actor.unlinearArmor, actor.expForDie);
            int[] array = new int[WayPointsControl.Instance.maxBranchs];
            for (int i = 0; i < array.Length; i++)
                array[i] = CryptoRandom.NextInt32(1, WayPointsControl.Instance.maxDepth + 1);
            way = WayPointsControl.Instance.BuildWay(array);
            spriteRn = GetComponentInChildren<SpriteRenderer>();
            spriteRn.sprite = sprite;
            rb = GetComponent<Rigidbody2D>();
            nameAndLevel = transform.GetChild(1).Find("Name and level").GetComponent<TextMeshProUGUI>();
            nameAndLevel.text = $"{Actor.name} <Lv {Actor.lv}>";

            visualModule = VisualModule.Instanse(transform, "Tower", Actor.AttackModule.range + Actor.GetAdvantageOfTheGround().range);
            visualModule.OnTriggerEnter += TriggerEnter;
            visualModule.OnTriggerExit += TriggerExit;

            closeVisualModule = VisualModule.Instanse(transform, "Tower", 0.4f);
            closeVisualModule.OnTriggerStay += CloseTriggerStay;

            tag = "Enemy";

            CurrentCountEnemies++;

            initialized = true;
        }

        public override void Kill()
        {
            base.Kill();
            CurrentCountEnemies--;
        }
        /// <summary>
        /// Устанавливает новый уровень для Актера
        /// </summary>
        /// <param name="newLv"></param>
        public abstract void LevelSet(int newLv);

        public bool wayComplete = false;
        protected int curentPoint = 0;
        private Vector2 direction;
        public Vector2 Direction { get => direction; }
        public bool CanMove { get; protected set; }
        public virtual void Move()
        {
            if(!CanMove)
            {
                direction = Vector2.zero;
                return;
            }
            direction = way[curentPoint].position - transform.position; direction.Normalize();
            rb.MovePosition((Vector2)transform.position + direction * Time.deltaTime * speed * Actor.GetAdvantageOfTheGround().speed);
            if (Vector2.Distance(transform.position, way[curentPoint].position) <= 0.1f)
                if (++curentPoint >= way.Length)
                {
                    wayComplete = true;
                }
            if(delayFromAdventages.Update(Time.deltaTime))
            {
                delayFromAdventages.Reset();
                Actor.GetCurrentGround(transform.position);

                visualModule.Range = Actor.AttackModule.range + Actor.GetAdvantageOfTheGround().range;
            }
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y + World.DeltaPosition.z);
        }

        protected virtual void TriggerEnter(object sender, EventArgs args)
        {

        }
        protected virtual void TriggerExit(object sender, EventArgs args)
        {

        }

        /// <summary>
        /// Вызывается при близости
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected virtual void CloseTriggerStay(object sender, EventArgs args)
        {
            
        }
    }

    

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;
using System;
using TMPro;
using Build;
using Control;
using Build.UI;
using static Maths.Utility;

namespace Actors.Tower
{
    public abstract class TowerBase : ActorBase, ISelectableObject, IBuilding
    {

        public static int CurrentCountTowers { get; protected set; }

        [Header("Build params")]
        public int buildPrice = 20;
        public float buildMaterialPrice = 5;
        public float buildTime = 10f;
        [SerializeField]
        protected string _path = "Offensive/Tower prefab";
        public string Path { get => _path; set => _path = value; }


        public virtual void Initilazie()
        {
            spriteRn = GetComponentInChildren<SpriteRenderer>();
            spriteRn.sprite = sprite;
            rb = GetComponent<Rigidbody2D>();
            nameAndLevel = transform.GetChild(1).Find("Name and level").GetComponent<TextMeshProUGUI>();
            nameAndLevel.text = $"{Actor.name} <Lv {Actor.lv}>";
            Actor.GetCurrentGround(transform.position);

            visualModule = VisualModule.Instanse(transform, "Enemy", Actor.AttackModule.range + Actor.GetAdvantageOfTheGround().range);
            visualModule.OnTriggerEnter += TriggerEnter;
            visualModule.OnTriggerExit += TriggerExit;

            Selectable.Instance.OnSelectEventHandler += Select;

            attackDelayTimer.Percent = 0;

            gameObject.name = Actor.name;
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y + World.DeltaPosition.z);
            Actor.LevelChangeHandler += LvUp;

            CurrentCountTowers++;
            col = GetComponent<BoxCollider2D>();

            initialized = true;
        }
        private BoxCollider2D col;
        public virtual bool Select(object sender, EventArgs args)
        {
            var selectPos = (args as EventSelectArgs).OnWorldSelectedPosition;
            if (Vector2.Distance(transform.position, selectPos) <= ((sender as Selectable).avalibleDistance + col.bounds.size.magnitude / 6f))
            {
                Selectable.Instance.SelectThisActor(transform, Actor);
                return true;
            }
            else return false;
        }

        public virtual (int buildPrice, float buildMaterialPrice, float buildTime) GetBuildData => (buildPrice, buildMaterialPrice, buildTime);

        public virtual (PowerImageName imageName, MorphsColors imageColor, float value)[] GetUIData =>
            new (PowerImageName imageName, MorphsColors imageColor, float value)[] {
                (PowerImageName.Heart, MorphsColors.True_red, (float)Actor.health.MaxValue),
                (PowerImageName.Fist, MorphsColors.Red, (float)Actor.AttackModule.Damage),
                (PowerImageName.Bow, MorphsColors.Green, (float)Actor.AttackModule.range),
                (PowerImageName.Lightning, MorphsColors.Blue, (float)Actor.AttackModule.AttacksPerMinute),
                (PowerImageName.Shield, MorphsColors.Yellow, (float)Actor.linearArmor)
            };

        protected virtual void TriggerEnter(object sender, EventArgs args)
        {

        }
        protected virtual void TriggerExit(object sender, EventArgs args)
        {

        }

        /// <summary>
        /// Вызывается при смерти
        /// </summary>
        public override void Kill()
        {
            base.Kill();
            //Debug.Log(Actor.name + " Destroy");
            Selectable.Instance.OnSelectEventHandler -= Select;
            CurrentCountTowers--;
        }

        /// <summary>
        /// Вызывается при повышении уровня
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public virtual void LvUp(object sender, System.EventArgs args)
        {
            nameAndLevel.text = $"{Actor.name} <Lv {Actor.lv}>";
        }

          
    }

   
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Control;
using System.Globalization;

namespace Maths
{

    public static class Utility
    {
        public enum MorphParametr
        {
            Default,
            Use_Sign,
            Unuse_Sign,
        }
        public static string MorphSingle(float value, int minNumbers = 2, MorphParametr morph = MorphParametr.Default)
        {
            if (value == 0)
                return "0";
            value += Mathf.Pow(0.1f, minNumbers) * (Mathf.Abs(value) / value);
            
            string buffer = Math.Abs(value).ToString("G", CultureInfo.InvariantCulture);
            int lengthToDot = buffer.IndexOf('.');
            if(lengthToDot < minNumbers)
            {
                minNumbers++;
            }
            else if(lengthToDot > minNumbers)
            {
                minNumbers = lengthToDot;
            }
            switch (morph)
            {
                case MorphParametr.Default:
                    if (value > 0)
                    {
                        return $"{buffer.Substring(0, minNumbers)}";
                    }
                    else if (value < 0)
                    {
                        return $"-{buffer.Substring(0, minNumbers)}";
                    }
                    else return $"{buffer.Substring(0, minNumbers)}";
                  
                case MorphParametr.Use_Sign:
                    if (value > 0)
                    {
                        return $"+{buffer.Substring(0, minNumbers)}";
                    }
                    else if (value < 0)
                    {
                        return $"-{buffer.Substring(0, minNumbers)}";
                    }
                    else return $"{buffer.Substring(0, minNumbers)}";
                  
                case MorphParametr.Unuse_Sign:
                    return $"{buffer.Substring(0, minNumbers)}";
                default: return null;
            }
 
        }
        public enum MorphsColors
        {
            White,
            Black,
            True_red,
            True_green,
            True_blue,
            Red,
            Green,
            Blue,
            Yellow,
            Light_red,
            Light_green,
            Light_blue,
            Light_yellow,
            Dark_red,
            Dark_green,
            Dark_blue,
            Dark_yellow,

        }

        public static Color MorphColor(MorphsColors color)
        {
            switch (color)
            {
                case MorphsColors.White:
                    return new Color(1f, 1f, 1f, 1f);
                case MorphsColors.Black:
                    return new Color(0.1f, 0.1f, 0.1f, 1f);
                case MorphsColors.True_red:
                    return new Color(1f, 0f, 0f, 1f);
                case MorphsColors.True_green:
                    return new Color(0f, 1f, 0f, 1f);
                case MorphsColors.True_blue:
                    return new Color(0f, 0f, 1f, 1f);
                case MorphsColors.Red:
                    return new Color(0.9339623f, 0.4138782f, 0.3480331f, 1f);
                case MorphsColors.Green:
                    return new Color(0.3592936f, 0.8679245f, 0.3152367f, 1f);
                case MorphsColors.Blue:
                    return new Color(0.2717159f, 0.3748039f, 0.8113208f, 1f);
                case MorphsColors.Yellow:
                    return new Color(1f, 1f, 0, 1f);
                case MorphsColors.Light_red:
                    return new Color(1f, 0.3781345f, 0.3647059f, 1f);
                case MorphsColors.Light_green:
                    return new Color(0.3632075f, 1f, 0.4422306f, 1f);
                case MorphsColors.Light_blue:
                    return new Color(0.3647059f, 0.5435221f, 1f, 1f);
                case MorphsColors.Light_yellow:
                    return new Color(0.9903116f, 1f, 0.3647059f, 1f);
                case MorphsColors.Dark_red:
                    return new Color(0.4433962f, 0.008211041f, 0f, 1f);
                case MorphsColors.Dark_green:
                    return new Color(0, 0.5283019f, 0.06522242f, 1f);
                case MorphsColors.Dark_blue:
                    return new Color(0, 0.1392966f, 0.490566f, 1f);
                case MorphsColors.Dark_yellow:
                    return new Color(0.4472397f, 0.4528302f, 0, 1f);
                default: return new Color();
            }
        }
    }
    [System.Serializable]
    public struct RangeValue
    {
        

        [SerializeField]
        private double value, minimalValue, maximalValue;
        private double minimalBound, maximalBound;

        /// <summary>
        /// Минимально допустимое значение
        /// </summary>
        public double MinValue
        {
            get
            {
                return minimalValue + minimalBound;
            }
            set
            {
                minimalBound = value - minimalValue;
                if (Value < MinValue)
                    Value = MinValue;
            }
        }

        /// <summary>
        /// Максимально допустимое значение
        /// </summary>
        public double MaxValue
        {
            get
            {
                return maximalValue + maximalBound;
            }
            set
            {
                maximalBound = value - maximalValue;
                if (Value > MaxValue)
                    Value = MaxValue;
            }
        }

        /// <summary>
        /// Сбрасывает границы к дефолтным значениям (при создании структуры)
        /// </summary>
        public void Reset()
        {
            minimalBound = maximalBound = 0;
            Value += 0;
        }

        /// <summary>
        /// Само значение структуры. Оно не может выйти за границы.
        /// </summary>
        public double Value
        {
            get
            {
                return value;
            }
            set
            {

                if (value > MaxValue)
                    this.value = MaxValue;
                else
                if (value < MinValue)
                    this.value = MinValue;
                else
                    this.value = value;
            }
        }

        /// <summary>
        /// Структура поддерживающая динамическую модификацию
        /// </summary>
        [System.Serializable]
        public struct ModificatedFloat
        {
            float baseValue;
            List<Singleton> mods;
            public float BaseValue
            {
                get => baseValue;
                set
                {
                    if (value == baseValue)
                        return;
                    baseValue = value;
                }
            }
            public float Value
            {
                get
                {
                    float modValue = 0;
                    foreach (var mod in mods)
                        modValue += mod.Value(baseValue);
                    return modValue + baseValue;
                }
            }

            public void Add(float addValue, float modValue)
            {
                mods.Add(new Singleton(addValue, modValue));
            }
            public bool Remove(float addValue, float modValue)
            {
                for(int i =0;i<mods.Count;i++)
                {
                    if (addValue - 0.01f >= mods[i].addValue && addValue + 0.01f <= mods[i].addValue)
                        if (modValue - 0.001f >= mods[i].modValue && modValue + 0.001f <= mods[i].modValue)
                        {
                            mods.RemoveAt(i);
                            return true;
                        }
                }
                return false;
            }

            public ModificatedFloat(float value)
            {
                baseValue = value;
                mods = new List<Singleton>(4);
            }

            private struct Singleton
            {
               
                public float addValue;
                public float modValue;

                public float Value(float basicValue)
                {
                   return addValue + basicValue * modValue;
                }

                public Singleton(float addValue, float modValue)
                {
                   
                    this.addValue = addValue;
                    this.modValue = modValue;
                }
            }
        }

        /// <summary>
        /// Изменяет границы структуры. Значение суммируется к изначальному
        /// </summary>
        /// <param name="minimal">Изменение минимальной границы</param>
        /// <param name="maximal">Изменение максимальной границы</param>
        public void SetBounds(float minimal, float maximal)
        {
            minimalBound = minimal; maximalBound = maximal;
            Value += 0;
        }

        /// <summary>
        /// Возвращает процентное значение от максимума. Устанавливаемое значение должно быть в диапазоне от 0 до 1
        /// </summary>
        public float Percent
        {
            get
            {
                return (float)(value / MaxValue) * 100f;
            }
            set
            {
                if (value > 1)
                    value = 1;
                if (value < 0)
                    value = 0;

                this.Value = ((MaxValue - MinValue) * value) + MinValue;
            }
        }

        public bool IsMin
        {
            get
            {
                if (value == MinValue)
                    return true;
                else return false;
            }
        }
        public bool IsMax
        {
            get
            {
                if (value == MaxValue)
                    return true;
                else return false;
            }
        }

        /// <summary>
        /// Обычный конструктор
        /// </summary>
        /// <param name="minimalValue"></param>
        /// <param name="maximalValue"></param>
        /// <param name="percentage">Изначальное значение Value в процентах</param>
        public RangeValue(double minimalValue, double maximalValue, float percentage =0)
        {
            minimalBound = maximalBound = 0;
            this.minimalValue = minimalValue;
            this.maximalValue = maximalValue;
            value = (maximalValue - minimalValue) * percentage + minimalValue;
        }

        /// <summary>
        /// Обычный конструктор со встроеным приведением
        /// </summary>
        /// <param name="minimalValue"></param>
        /// <param name="maximalValue"></param>
        /// <param name="percentage">Изначальное значение Value в процентах</param>
        public RangeValue(float minimalValue, float maximalValue, float percentage = 0)
        {
            minimalBound = maximalBound = 0;
            this.minimalValue = minimalValue;
            this.maximalValue = maximalValue;
            value = (maximalValue - minimalValue) * percentage + minimalValue;
        }
    }

    /// <summary>
    /// Класс таймера для использования в Unity
    /// </summary>
    [System.Serializable]
    public struct Timer
    {
        RangeValue time;
        public bool countDown;
        public bool Update(float k)
        {
            if (!countDown)
                return false;
            time.Value -= k;
            return time.IsMin;
        }
        public float GetTime
        {
            get
            {
                return (float)time.Value;
            }
        }
        public void Reset()
        {
            countDown = true;
            time.Value = time.MaxValue;
        }
        public void Reset(float newDelay)
        {
            countDown = true;
            time.MaxValue = newDelay;
            time.Value = time.MaxValue;
        }
        public float Percent
        {
            get
            {
                return time.Percent/100f;
            }
            set
            {
                time.Percent = value;
            }
        }
        /// <summary>
        /// Создает новый таймер на указаный промежуток времени
        /// </summary>
        /// <param name="delay"></param>
        public Timer(float delay)
        {
            time = new RangeValue(0, delay, 1);
            countDown = true;
        }
    }

    [System.Serializable]
    public struct IntRangeValue
    {
        [SerializeField]
        private int value, minimalValue, maximalValue;
        private int minimalBound, maximalBound;

        /// <summary>
        /// Минимально допустимое значение
        /// </summary>
        public int MinValue
        {
            get
            {
                return minimalValue + minimalBound;
            }
            set
            {
                minimalBound = value - minimalValue;
                if (Value < MinValue)
                    Value = MinValue;
            }
        }

        /// <summary>
        /// Максимально допустимое значение
        /// </summary>
        public int MaxValue
        {
            get
            {
                return maximalValue + maximalBound;
            }
            set
            {
                maximalBound = value - maximalValue;
                if (Value > MaxValue)
                    Value = MaxValue;
            }
        }

        /// <summary>
        /// Сбрасывает границы к дефолтным значениям (при создании структуры)
        /// </summary>
        public void Reset()
        {
            minimalBound = maximalBound = 0;
            Value += 0;
        }

        /// <summary>
        /// Само значение структуры. Оно не может выйти за границы.
        /// </summary>
        public int Value
        {
            get
            {
                return value;
            }
            set
            {

                if (value > MaxValue)
                    this.value = MaxValue;
                else
                if (value < MinValue)
                    this.value = MinValue;
                else
                    this.value = value;
            }
        }

        /// <summary>
        /// Изменяет границы структуры. Значение суммируется к изначальному
        /// </summary>
        /// <param name="minimal">Изменение минимальной границы</param>
        /// <param name="maximal">Изменение максимальной границы</param>
        public void SetBounds(int minimal, int maximal)
        {
            minimalBound = minimal; maximalBound = maximal;
            Value += 0;
        }

        /// <summary>
        /// Возвращает процентное значение от максимума. Только для чтения
        /// </summary>
        public float Percent
        {
            get
            {
                return (value / (float)MaxValue) * 100f;
            }
        }

        public bool IsMin
        {
            get
            {
                if (value == MinValue)
                    return true;
                else return false;
            }
        }
        public bool IsMax
        {
            get
            {
                if (value == MaxValue)
                    return true;
                else return false;
            }
        }

        /// <summary>
        /// Обычный конструктор
        /// </summary>
        /// <param name="minimalValue"></param>
        /// <param name="maximalValue"></param>
        /// <param name="percentage">Изначальное значение Value в процентах</param>
        public IntRangeValue(int minimalValue, int maximalValue, float percentage = 0)
        {
            minimalBound = maximalBound = 0;
            this.minimalValue = minimalValue;
            this.maximalValue = maximalValue;
            value = (int)((maximalValue - minimalValue) * percentage + minimalValue);
        }

        public static implicit operator RangeValue(IntRangeValue value)
        {
            return new RangeValue(value.MinValue, value.MaxValue, value.Percent/100f);
        }
        public static explicit operator IntRangeValue(RangeValue value)
        {
            return new IntRangeValue((int)value.MinValue, (int)value.MaxValue, value.Percent / 100f);
        }
    }
}

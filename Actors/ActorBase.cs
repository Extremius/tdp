﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;
using System;
using TMPro;
using Build;
using Control;

namespace Actors
{
    public abstract class ActorBase : MonoBehaviour
    {
        public abstract Actor Actor { get; }

        [Header("Main")]
        protected Rigidbody2D rb;
        protected VisualModule visualModule;

        protected SpriteRenderer spriteRn;
        protected TextMeshProUGUI nameAndLevel;
        public Sprite sprite;
        protected Timer attackDelayTimer;
        protected bool initialized = false;
        public readonly Tag ActorTag = new Tag();

        public event EventHandler KillActorHandler;
        /// <summary>
        /// Убивает Актера. Определите этот метод в дочерних классах
        /// </summary>
        public virtual void Kill()
        {
            KillActorHandler?.Invoke(this, null);
        }
        protected Timer updateDelay = new Timer(0.2f);

        [System.Serializable]
        public class Tag
        {
            private List<string> tags = new List<string>(4);

            public void AddTag(string newTag)
            {
                if (!Exist(newTag))
                    tags.Add(newTag);
            }
            public void AddTagsArray(params string[] newTagsArray)
            {
                foreach (var newTag in newTagsArray)
                    AddTag(newTag);
            }
            public bool RemoveTag(string tag)
            {
                return tags.Remove(tag);
            }
            public bool Exist(string tag)
            {
                foreach (var tg in tags)
                    if (tag.Equals(tg))
                        return true;
                return false;
            }
            public bool AnyExist(params string[] tag)
            {
                foreach (var tg in tags)
                    foreach (var _tg in tag)
                        if (tg.Equals(_tg))
                            return true;
                return false;
            }
            public bool AllExist(params string[] tag)
            {
                bool[] contain = new bool[tag.Length];
                int startJ = 0;
                for(int i =0;i<tags.Count;i++)
                {
                    for(int j=startJ;j<tag.Length;j++)
                    {
                        if(tags[i].Equals(tag[j]))
                        {
                            startJ = j + 1;
                            contain[j] = true;
                        }
                    }
                }
                bool allExists = true;
                foreach (var cnt in contain)
                    if (!cnt)
                        allExists = false;
                return allExists;
            }
        }
    }
}

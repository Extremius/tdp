﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Actors;
using System.Reflection;
using Maths;
using TMPro;
public class UIPropertySlider : MonoBehaviour 
{
    TextMeshProUGUI text, extraInfoText;
    Slider slider, deltaSlider;
    public Actor tracedActor;
    public string tracedProperty;
    public float speed = 8;
    double avalibleDelta = 0.001f;
    FieldInfo property;
    RangeValue value;
    RangeValue localValue, deltaLocalValue;
    Timer deltaTimer = new Timer(0.7f);
    Camera mainCam;
    [Header("Extras")]
    public bool extraInfo = true;
    public float minDelta = 0.19f;
    public float extraValue;
    Vector3 targetPos = new Vector3(0, 65f);
    Timer extraTimer = new Timer(6f);
    Animator extraAnim;
    void Init(int id)
    {
        if (tracedActor == null)
            return;
        property = tracedActor.GetType().GetField(tracedProperty);
        value = (RangeValue)property.GetValue(tracedActor);
        localValue = deltaLocalValue = value;
        text = transform.GetChild(1).GetChild(id).GetChild(2).GetComponent<TextMeshProUGUI>();
        text.text = $"{value.Value : 0} / {value.MaxValue : 0}";
        if (slider ==null)
            slider = transform.GetChild(1).GetChild(id).GetComponent<Slider>();
        if (deltaSlider == null)
            deltaSlider = transform.GetChild(1).GetChild(id).GetChild(0).GetComponent<Slider>();
        slider.maxValue = 100; slider.minValue = 0; slider.value = (float)value.Percent;
        deltaSlider.minValue = 0; deltaSlider.maxValue = 100; deltaSlider.value = value.Percent;

        if (extraInfo)
        {
            extraInfoText = Instantiate(Resources.Load<GameObject>("Prefabs/DmgTxtInfo")).GetComponentInChildren<TextMeshProUGUI>();
            extraInfoText.transform.parent.SetParent(transform.Find("ActorUI"),false);
            extraInfoText.transform.parent.position = slider.transform.position;
            extraInfoText.name = $"ExtraInfo {tracedProperty}";      
            extraInfoText.color = slider.transform.Find("Fill").GetComponent<Image>().color;
            extraAnim = extraInfoText.transform.parent.GetComponent<Animator>();
            ResetExtra();
        }
    }
    double delta = 0;
    float maxSize = 6f, maxDistance = 12;
    public bool EnableUpdate { get; private set; }
    private void Update()
    {
        if (speed <= 0)
            return;
        if (tracedActor == null)
            return;

        if (!EnableUpdate)
        {
            bool distanceFlag = false;
            float distance = Vector2.Distance(mainCam.transform.position, transform.position);
            if (distance <= maxDistance)
            {
                distanceFlag = true;
               // EnableUpdate = true;
               // UpdateGraphics(true);
            }
            if(distanceFlag && mainCam.orthographicSize <= maxSize)
            {
                EnableUpdate = true;
                UpdateGraphics(true);
            }
            return;
        }
        else
        {
            bool distanceFlag = false;
            float distance = Vector2.Distance(mainCam.transform.position, transform.position);
            if (distance > maxDistance)
            {
                distanceFlag = true;
                // EnableUpdate = true;
                // UpdateGraphics(true);
            }
            if (distanceFlag || mainCam.orthographicSize > maxSize)
            {
                EnableUpdate = false;
                UpdateGraphics(false);
                return;
            }


            //MAIN
            if (extraInfo)
            {
                var trueValue = (RangeValue)property.GetValue(tracedActor);
                float trueDelta = (float)(trueValue.Value - value.Value);
                //Debug.Log(trueDelta + " Delta");
                if (Mathf.Abs(trueDelta) >= minDelta)
                {
                   
                    if(!extraTimer.countDown || !extraInfoText.gameObject.activeInHierarchy)
                    {
                        CullExtra();
                    }

                    extraValue += trueDelta;
                    extraInfoText.text = $"{Utility.MorphSingle(extraValue, 2, Utility.MorphParametr.Use_Sign)} ({Utility.MorphSingle(trueDelta, 2, Utility.MorphParametr.Use_Sign)})";
                    extraTimer.Reset();
                    //extraInfoText.transform.position = slider.transform.position;
                    extraAnim.Play("Main");
                }
                if (!extraTimer.Update(Time.deltaTime) && extraTimer.countDown)
                {
                    extraInfoText.color = ChangeColorAlpha(extraInfoText.color, extraTimer.Percent);
                    //extraInfoText.transform.position += (slider.transform.position - targetPos) * Time.deltaTime * 2f;
                }
                else
                {
                    ResetExtra();
                }
            }


            value = (RangeValue)property.GetValue(tracedActor);
            delta = value.Value - localValue.Value;
            text.text = $"{value.Value: 0} / {value.MaxValue: 0}";
            if (System.Math.Abs(delta) > avalibleDelta)
            {
                //Debug.Log("Change Complete");
                localValue.Value += delta * Time.deltaTime * speed;
                slider.value = localValue.Percent;

                if (delta < 0)
                    deltaTimer.Reset();
            }

            if (deltaTimer.Update(Time.deltaTime))
            {
                delta = value.Value - deltaLocalValue.Value;
                if (System.Math.Abs(delta) > avalibleDelta)
                {
                    deltaLocalValue.Value += delta * Time.deltaTime * speed;
                    deltaSlider.value = deltaLocalValue.Percent;
                }
            }
        }
    }

    private void OnKillActor(object sender, System.EventArgs args )
    {
        if(extraInfo && EnableUpdate)
        {
            extraAnim.Play("Main");
            var root = extraInfoText.transform.parent.parent;
            root.parent = null;
            root.position = tracedActor.Root.transform.position;

            for(int i =0;i<root.childCount;i++)
            {
                if(!root.GetChild(i).name.Contains("DmgTxtInfo"))
                {
                    root.GetChild(i).gameObject.SetActive(false);
                }
            }

            var trueValue = (RangeValue)property.GetValue(tracedActor);
            float trueDelta = (float)(trueValue.Value - value.Value);
            //Debug.Log(trueDelta + " Delta");
            if (Mathf.Abs(trueDelta) >= minDelta)
            {

                if (!extraTimer.countDown || !extraInfoText.gameObject.activeInHierarchy)
                {
                    CullExtra();
                }

                extraValue += trueDelta;
                extraInfoText.text = $"{Utility.MorphSingle(extraValue, 2, Utility.MorphParametr.Use_Sign)} ({Utility.MorphSingle(trueDelta, 2, Utility.MorphParametr.Use_Sign)})";
                extraTimer.Reset();
                //extraInfoText.transform.position = slider.transform.position;
                extraAnim.Play("Main");
            }
            if (!extraTimer.Update(Time.deltaTime) && extraTimer.countDown)
            {
                extraInfoText.color = ChangeColorAlpha(extraInfoText.color, extraTimer.Percent);
                //extraInfoText.transform.position += (slider.transform.position - targetPos) * Time.deltaTime * 2f;
            }
            else
            {
                ResetExtra();
            }

            root.gameObject.AddComponent<DestroyCoroutine>().InitDestroy(2f);
        }

        tracedActor.Root.GetComponent<ActorBase>().KillActorHandler -= OnKillActor;
    }
    
   
    private void CullExtra()
    {
        extraInfoText.transform.parent.gameObject.SetActive(true);
        //extraInfoText.transform.localPosition = new Vector2(0, 20f);
        extraTimer.Reset();
        
        extraValue = 0;
    }
    private void ResetExtra()
    {
        extraTimer.countDown = false;
        extraInfoText.transform.parent.gameObject.SetActive(false);
    }
    private Color ChangeColorAlpha(Color original, float newAlpha)
    {
        original.a = newAlpha;
        return original;
    }

    private void UpdateGraphics(bool result)
    {
        slider.gameObject.SetActive(result);
        //text.gameObject.SetActive(result);
    }

    public void SetData(Actor target, string tracedProperty,int sliderId =0, bool extraInfo = false)
    {
        tracedActor = target;
        this.tracedProperty = tracedProperty;
        Init(sliderId);
        mainCam = Camera.main;
        //oldSize = mainCam.orthographicSize;
        this.extraInfo = extraInfo;
        target.Root.GetComponent<ActorBase>().KillActorHandler += OnKillActor;
        EnableUpdate = true;
    }

    public void ForcedUpdate()
    {
        value = (RangeValue)property.GetValue(tracedActor);
        //delta = value.Value - localValue.Value;
        text.text = $"{value.Value: 0} / {value.MaxValue: 0}";

        //Debug.Log("Change Complete");
        localValue = value;
        slider.value = localValue.Percent;
        deltaTimer.Reset();
        deltaLocalValue = value;
        deltaSlider.value = deltaLocalValue.Percent;


    }
}

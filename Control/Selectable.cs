﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Maths;

namespace Control
{
    public class Selectable : MonoBehaviour
    {
        const float deltaTimeSet = 0.15f;
        public static Selectable Instance;
        public float avalibleDistance = 0.45f;
        public event EventSelectHandler OnSelectEventHandler;
        public event EventHandler OnDoubleClick;
        public delegate bool EventSelectHandler(object sender, EventArgs args);
        Timer timer = new Timer(0.15f),
            doubleClickTimer = new Timer(0.35f),
            dragDelay = new Timer(0.1f);
        [Header("Select graphics")]
        public Transform selectGraphic;
        public SelectorHelpUI unselectImage;
        private List<IGUI> gUIs = new List<IGUI>(5);
        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else if (Instance != this)
                Destroy(gameObject);

            unselectImage.gameObject.SetActive(false);

            timer.countDown = false;
            doubleClickTimer.countDown = false;
        }
        bool mouseUp = true, pressed = false; Vector2 oldMousePos;
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                timer.countDown = true;
                doubleClickTimer.countDown = true;
                oldMousePos = Input.mousePosition;
                mouseUp = false;
            }

            if(!Dragging && !mouseUp  && oldMousePos != (Vector2)Input.mousePosition && dragDelay.Update(Time.deltaTime))
            {
                Dragging = true;
                dragDelay.Reset();
                //Debug.Log("Dragging enable");
            }

            if (!doubleClickTimer.Update(Time.deltaTime) && doubleClickTimer.countDown && !UnSelectable)
            {
                
                if (Input.GetMouseButtonUp(0))
                {
                    pressed = true;
                    mouseUp = true;
                    Dragging = false;
                    //Debug.Log("Dragging disable");
                }

                if (Input.GetMouseButtonDown(0) && pressed)
                {
                    OnDoubleClick?.Invoke(this, new EventSelectArgs(Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono)));
                    doubleClickTimer.Reset();
                    doubleClickTimer.countDown = false;
                    mouseUp = false;
                    pressed = false;
                    
                }
            }
            else if (doubleClickTimer.countDown)
            {
                doubleClickTimer.Reset();
                doubleClickTimer.countDown = false;
                //mouseUp = false;
                pressed = false;
            }
            

            if (!timer.Update(Time.deltaTime) && Input.GetMouseButtonUp(0) && !UnSelectable)
            {
                Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
                selectGraphic.gameObject.SetActive(false);
                tracedPos = null;
                SelectedUIInfo.Instance.DeselectActor();
                OnSelectEventHandler?.Invoke(this, new EventSelectArgs(pos));

                timer.Reset(deltaTimeSet + Time.deltaTime);
                timer.countDown = false;
                Dragging = false;
               // Debug.Log("Dragging disable");
                //Debug.Log($"Send pos {pos.ToString()}");
            }
            else if (Input.GetMouseButtonUp(0))
            {
                timer.Reset(deltaTimeSet + Time.deltaTime);
                timer.countDown = false;
                
            }

            if (Input.GetMouseButtonUp(0))
            {
                mouseUp = true;
                Dragging = false;
                //Debug.Log("Dragging disable");
                oldMousePos = Input.mousePosition;
            }

            /*if (tracedPos != null)
            {
                selectGraphic.transform.position = tracedPos.position;
                UnSelectable = true;
            }*/

        }
        public bool Dragging { get; private set; }
        Transform tracedPos;
        public bool Tracing { get => tracedPos != null; }
        public void SelectThisActor(Transform pos, Actors.Actor actor = null)
        {
            selectGraphic.gameObject.SetActive(true);
            selectGraphic.position = pos.position;
            tracedPos = pos;
            UnSelectable = true;
            if(actor!=null)
            {
                float range = actor.AttackModule.range + actor.GetAdvantageOfTheGround().range;
                if (range < 0)
                    range = 0;
               selectGraphic.GetChild(0).localScale = new Vector3(range*2,range*2,1);
            }
        }
        bool _unselectable = false;
        public bool UnSelectable
        {
            get
            {
                return _unselectable;
            }
            set
            {
                if(_unselectable!=value)
                {
                    unselectImage.gameObject.SetActive(value);
                    _unselectable = value;
                }
            }
        }

        internal List<IGUI> GUIs { get => gUIs; set => gUIs = value; }

        public void DeselectAll()
        {
            foreach (var ui in GUIs)
                ui.Close();
        }
        
    }

    

    interface ISelectableObject
    {
        bool Select(object sender, EventArgs args);
    }
    interface IGUI
    {
        void Close();
    }
    class EventSelectArgs : EventArgs
    {
        public Vector2 OnWorldSelectedPosition;

        public EventSelectArgs(Vector2 onWorldSelectedPosition)
        {
            OnWorldSelectedPosition = onWorldSelectedPosition;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugWayPrinter : MonoBehaviour
{
    public Transform[] way;
    public int[] depth = { 1 };
    public Vector3 deltaPos = new Vector3(0, 0, -1);
    private void Start()
    {
        way = WayPointsControl.Instance.BuildWay(depth);
        var line =GetComponent<LineRenderer>();
        line.positionCount = way.Length;
        List<Vector3> pt = new List<Vector3>(12);
        foreach(var p in way)
        {
            pt.Add(p.position + deltaPos);
        }
        line.SetPositions(pt.ToArray());
    }
}

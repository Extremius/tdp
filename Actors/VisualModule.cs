﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Actors
{
    public class VisualModule : MonoBehaviour
    {
        CircleCollider2D trigger;
        public string tracedTag;
        public event EventHandler OnTriggerEnter;
        public event EventHandler OnTriggerExit;
        public event EventHandler OnTriggerStay;
        public float Range { get => trigger.radius;
            set
            {
                if (value < 0)
                    trigger.radius = 0;
                else
                    trigger.radius = value;
            }
        }
        public static VisualModule Instanse(Transform parent, string tracedTag, float radius)
        {
            GameObject go = new GameObject("Visual Module", typeof(CircleCollider2D), typeof(VisualModule));
            go.transform.parent = parent;
            go.transform.position = parent.position;
            var component = go.GetComponent<VisualModule>();
            component.trigger = go.GetComponent<CircleCollider2D>();
            component.trigger.isTrigger = true;
            component.Range = radius;
            component.tracedTag = tracedTag;

            return component;
        }

        public Collider2D[] Cast()
        {
          var cols = new List<Collider2D>(Physics2D.OverlapCircleAll(transform.position, Range));
            List<int> refId = new List<int>(cols.Count);
            foreach (var col in cols)
            {
                if (col.tag != tracedTag)
                    refId.Add(cols.FindIndex(x => x == col));
                if (col.isTrigger == true && !refId.Contains(cols.FindIndex(x => x == col)))
                    refId.Add(cols.FindIndex(x => x == col));
            }
            List<Collider2D> clearCols = new List<Collider2D>(cols.Count - refId.Count);
            if (refId.Count > 0)
            {
                for(int i =0;i<cols.Count;i++)
                {
                    if(!refId.Contains(i))
                    clearCols.Add(cols[i]);
                }
            }
            return clearCols.ToArray();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if(collision.tag == tracedTag)
                OnTriggerEnter?.Invoke(this, new EventTriggerArgs(collision));
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == tracedTag)
                OnTriggerExit?.Invoke(this, new EventTriggerArgs(collision));
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.tag == tracedTag)
                OnTriggerStay?.Invoke(this, new EventTriggerArgs(collision));
        }

    }
    class EventTriggerArgs : EventArgs
    {
        public readonly Collider2D collision;

        public EventTriggerArgs(Collider2D collision)
        {
            this.collision = collision;
        }
    }
}

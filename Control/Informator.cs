﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;

/// <summary>
/// Информирует игрока различными сообщениями из среды
/// </summary>
public class Informator : MonoBehaviour
{
    static Informator Instance;
    Animator anim;
    Timer animTimer = new Timer(1);
    Timer activeTimer = new Timer(1.5f);
    List<(string msg, Color color)> messages = new List<(string msg, Color color)>(6);

    TMPro.TextMeshProUGUI text;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }
    private void Start()
    {
        anim = GetComponent<Animator>();
        //activeTimer.Percent = 0;
       
        text = GetComponentInChildren<TMPro.TextMeshProUGUI>();
        
        gameObject.SetActive(false);
    }
    public enum AvalibleColors
    {
        Light_Green,
        Light_Yellow,
        Light_Red,
    }
    static Color GetColor(AvalibleColors color)
    {
        switch (color)
        {
            case AvalibleColors.Light_Green:
                return new Color(0.3632075f, 1f, 0.4422306f, 1f);
            case AvalibleColors.Light_Yellow:
                return new Color(0.9903116f, 1f, 0.3647059f, 1f);
            case AvalibleColors.Light_Red:
                return new Color(1, 0.3781345f, 0.3647059f, 1f);

            default:
                return Color.red;
        }
    }
    public static void SendMessage(string msg, AvalibleColors color = AvalibleColors.Light_Green)
    {
        if (Instance.messages.Count > 6)
            return;

        foreach (var tmp in Instance.messages)
            if (tmp.msg.Equals(msg))
                return;
        Instance.messages.Add((msg, GetColor(color)));
        Instance.gameObject.SetActive(true);
        if(Instance.messages.Count ==1)
        {
            Instance.activeTimer.Reset();
            Instance.text.color = Instance.messages[0].color;
            Instance.text.text = Instance.messages[0].msg;
            Instance.anim.speed = 0;
        }
    }
    string currentMsg;
    private void Update()
    {
        if (messages.Count <= 0)
        {
            gameObject.SetActive(false);
            return;
        }
       

        if(activeTimer.Update(Time.deltaTime))
        {
            anim.speed = 1;
            if(animTimer.Update(Time.deltaTime))
            {
                messages.RemoveAt(0);
                if(messages.Count>0)
                {
                    text.color = messages[0].color;
                    text.text = messages[0].msg;
                    activeTimer.Reset();

                    anim.Play(0);
                    anim.speed = 0;
                    animTimer.Reset();
                }
                else
                {
                   
                    gameObject.SetActive(false);
                }

                
            }
        }
    }

}

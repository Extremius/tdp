﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Maths;

namespace Actors.Tower
{
    public class TowerBullet : MonoBehaviour
    {
        public TowerBase owner;
        Rigidbody2D rb;
        SpriteRenderer sprRnd;
        Sprite sprite;
        string tag; int capasity = 1;
        private Timer timeToDestroy = new Timer(120);

        public static TowerBullet InstanceBullet(TowerBase owner, Sprite sprite ,string tag,int capasity,(Vector2 direction, float speed) move)
        {
            GameObject bullet = new GameObject(owner.name + " bullet", typeof(TowerBullet), typeof(Rigidbody2D), typeof(BoxCollider2D), typeof(SpriteRenderer) );
            bullet.GetComponent<SpriteRenderer>().sprite = sprite;
            TowerBullet component = bullet.GetComponent<TowerBullet>();
            bullet.GetComponent<BoxCollider2D>().size = Vector3.one * 0.1f;
            bullet.transform.right = move.direction;
            bullet.layer = LayerMask.NameToLayer("Bullet");
            component.owner = owner;
            component.sprite = sprite;
            component.rb = bullet.GetComponent<Rigidbody2D>();
            component.rb.useAutoMass = true;
            component.rb.velocity = move.direction * move.speed;
            component.rb.gravityScale = 0; component.rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            component.tag = tag;
            if (capasity < 1)
                capasity = 1;
            component.capasity = capasity;

            return component;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.gameObject.tag == tag)
            {
                capasity--;
                (owner.Actor as IDamage).SetDamage(collision.gameObject.GetComponent<Enemy.EnemyBase>().Actor, new DamageParams());
                if (capasity <= 0)
                    Destroy(gameObject);
            }
        }

        private void Update()
        {
            if(timeToDestroy.Update(Time.deltaTime))
            {
                Destroy(gameObject);
            }
        }
    }
}

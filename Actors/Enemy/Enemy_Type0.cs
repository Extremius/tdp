﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Actors.Tower;
using Maths;
using System;
using Control;

namespace Actors.Enemy
{
    public sealed class Enemy_Type0 : EnemyBase
    {
        [Header("Enemy")]
        public Enemy_0 actor = new Enemy_0(null, "Goblin", 3, 0.5f, 0f,
            ((0.3f, DamageParams.DamageType.Physics), (10f, 2f), 0.5f, 60f),
            1, 3f
            );
        UIPropertySlider healthSlider;

        private float baseHealth, baseLinearArmor, baseDamage, baseMoney, baseExp; //BASE

        TowerBase target;
        public override Actor Actor => actor;

        public override void Initialize()
        {
            //pre main init
            actor = new Enemy_0(gameObject, actor);
            healthSlider = gameObject.AddComponent<UIPropertySlider>();
            healthSlider.SetData(actor, "health", 0, true);

            baseHealth = (float)Actor.health.MaxValue;
            baseLinearArmor = Actor.linearArmor;
            baseDamage = Actor.AttackModule.Damage;
            baseMoney = actor.moneyForDie;
            baseExp = actor.expForDie;
            //Selector
            //Selectable.Instance.OnSelectEventHandler += Select;



            attackDelayTimer = new Maths.Timer(60f / actor.AttackModule.AttacksPerMinute);
            attackDelayTimer.Percent = 0;

            base.Initialize();

            //post main init

            visualModule.Range = Actor.AttackModule.range;
        }

        void Start()
        {
            if (!initialized)
                Initialize();
        }


        public override void LevelSet(int newLv)
        {
            int delta = newLv - Actor.lv;

            Actor.health.MaxValue += baseHealth * delta * 0.6f;
            Actor.health.Percent = 1;

            Actor.linearArmor += baseLinearArmor * delta * 0.28f;
            Actor.AttackModule.Damage += baseDamage * delta * 0.3f;
            actor.expForDie += baseExp * delta * 0.1f;
            actor.moneyForDie += (int)(baseMoney * delta * 0.11f);
            Actor.lv = newLv;

            healthSlider.ForcedUpdate();

            nameAndLevel.text = $"{Actor.name} <Lv {Actor.lv}>";
        }

        // Update is called once per frame
        void Update()
        {
            if (!wayComplete)
                Move();
            if(!CanMove)
            {
                if (target == null)
                    CanMove = true;
            }
            Attack();
        }
        public override void Kill()
        {
            base.Kill();
            World.Instanse.money += actor.moneyForDie;
            //Selectable.Instance.OnSelectEventHandler -= Select;
            Destroy(gameObject);
        }


        protected override void TriggerEnter(object sender, EventArgs args)
        {
            if (!CanMove)
                return;
            var collision = (args as EventTriggerArgs).collision;

            target = collision.gameObject.GetComponent<TowerBase>();
        
        }
        protected override void TriggerExit(object sender, EventArgs args)
        {
            if (!CanMove)
                return;
            var collision = (args as EventTriggerArgs).collision;
            if(target!=null)
                if (target?.gameObject == collision?.gameObject)
                    target = null;
        }

       
        protected override void CloseTriggerStay(object sender, EventArgs args)
        {
            //base.CloseTriggerStay(sender, args);
            bool updating = false;
            if (CanMove && target != null)
            {
                updating = true;
                CanMove = false;
            }
            else if(!CanMove && target == null)
            {
                updating = true;
            }
           

            if(updating)
            {
                var collision = (args as EventTriggerArgs).collision;
                if (target != null)
                {
                    if (Vector2.Distance(target.transform.position, transform.position) > Vector2.Distance(collision.transform.position, transform.position))
                        target = collision.gameObject.GetComponent<TowerBase>();
                }
                else
                    target = collision.gameObject.GetComponent<TowerBase>();
            }
        }

        void Attack()
        {
            if (target == null)
                return;
            if(attackDelayTimer.Update(Time.deltaTime))
            {
                attackDelayTimer.Reset(60f / actor.AttackModule.AttacksPerMinute);

                (Actor as IDamage).SetDamage(target.Actor, new DamageParams(Actor, Actor.AttackModule.Damage, Actor.AttackModule.Crit, Actor.AttackModule.damageType));
            }
        }

        /*public bool Select(object sender, EventArgs args)
        {
            var selectPos = (args as EventSelectArgs).OnWorldSelectedPosition;
            if (Vector2.Distance(transform.position, selectPos) <= (sender as Selectable).avalibleDistance)
            {
                Selectable.Instance.SelectThisActor(transform,Actor);

                SelectedUIInfo.Instance.SelectActor(Actor, sprite, (nameof(Actor.health), "Health", SelectedUIInfo.AvalibleColors.Light_Green));
                return true;
            }
            else return false;
        }*/
    }

    [System.Serializable]
    public sealed class Enemy_0 : Actor, IDamage
    {

        public float expForDie = 5;
        public int moneyForDie = 3;

        public Enemy_0(GameObject Root, Actor origin) : base(Root, origin)
        {
            expForDie = (float)origin.exp.MaxValue;
            moneyForDie = (origin as Enemy_0).moneyForDie;
        }

        public Enemy_0(GameObject Root, string name, float health, float linearArmor, float unlinearArmor, ((float damage, DamageParams.DamageType type) dmgParams, (float chanse, float mult) crit, float range, float attackPerMinute) attackModule, int lv = 1, float baseEXP = 5) : base(Root, name, health, linearArmor, unlinearArmor, attackModule, lv, baseEXP)
        {
            expForDie = baseEXP;         
        }

        bool IDamage.GetDamage<T>(T damageOwner, DamageParams @params)
        {
            if (!IsAlive)
                return false;
            var advantage = GetAdvantageOfTheGround(); 
            float delta = (@params.GetTrueDamage(healthType) - (linearArmor + advantage.linear)) * (1 - (unlinearArmor + advantage.unlinear));
            if (delta < DamageParams.minDamage)
                delta = DamageParams.minDamage;
            health.Value -= delta ;
            if (!IsAlive)
            {
                damageOwner.AddEXP(expForDie);
    
                Root.GetComponent<EnemyBase>().Kill();
                return true;
            }
            return false;
        }

        void IDamage.SetDamage<T>(T target, DamageParams @params)
        {
            (target as IDamage).GetDamage(target, @params);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Security.Cryptography;

public static class CryptoRandom
{
    public static int NextInt32(int minValue, int maxValue)
    {
        int seed = 0;
        using (RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider())
        {
            byte[] rno = new byte[4];
            rg.GetBytes(rno);
            seed = BitConverter.ToInt32(rno, 0);
        }
        int rnd = new System.Random(seed).Next(minValue, maxValue);
        //Debug.Log("from " + minValue + " to " + maxValue + " rnd => " + rnd);
        return rnd;
    }
    public static float NextFloat(float minValue, float maxValue)
    {
        long seed = 0;
        using (RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider())
        {
            byte[] rno = new byte[8];
            rg.GetBytes(rno);
            seed = BitConverter.ToInt64(rno, 0);
        }
        double rnd = new System.Random((int)seed).NextDouble();
        float trueRnd = (float)rnd * (maxValue - minValue) + minValue;
        //Debug.Log("from " + minValue + " to " + maxValue + " rnd => " + trueRnd);
        return trueRnd;
    }

    public static bool Chanse1(float percent)
    {
        float curent = NextFloat(0, 1f);
        if (percent >= curent)
            return true;
        else return false;
    }
    public static bool Chanse100(float percent)
    {
        float curent = NextFloat(0, 100f);
        if (percent >= curent)
        {
            //Debug.Log("Critical!");
            return true;
        }
        else return false;
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Control;

namespace Build.UI
{
    public enum PowerImageName
    {
        Heart,
        Fist,
        Bow,
        Lightning,
        Shield,
        Pickaxe,
    }
    
    public class BuilderManagerUI : MonoBehaviour, IGUI
    {
        
        enum AvalibleSlots {x2 =2, x3, x4, x5, x6 }
        enum Mode {folders, towers };
        [SerializeField] RectTransform[] Lines;
        [SerializeField] AvalibleSlots slots = AvalibleSlots.x2; //slots
        [SerializeField] Transform[] buttons; //5 and 6 id is 6 button
        [SerializeField] UIButton[] funcButtons;
        [SerializeField] UIButton NextButton, PrewButton;
        Dictionary<int, int> buttonsData = new Dictionary<int, int>(6);
        //Folders
        [SerializeField] List<Folder> folders;
        
        Mode curentMode = Mode.folders;
        private int curentFolder = -1, curentList = 0, maxLists = 1, onLastList = 1,
            oldFolderList = -1; //Inits in load
        private const int maxOnList = 6;
        private Vector2 worldPoint = Vector2.zero;
        [Header("BasicSpr")]
        public Sprite folderSpr;
        [Header("CicrleBlueprint")]
       
        public Image mainSprt;
        public TextMeshProUGUI mainName, goldValue, materialValue, buildValue;
        //Powers
        public Image[] powerImgs;
        public TextMeshProUGUI[] powerValues;

        [Header("Resources")]
        public Sprite[] powerSrites; // 0 - hp, 1 - dmg, 2 - range, 3 - speed, 4 - armor
        

        private (int start, int value, int end) allowId = (0, 0, 5);
        private int NextId()
        {
            int value = allowId.value;
            allowId.value++;
            if (allowId.value > allowId.end)
                allowId.value = allowId.end;
            return value;
        }
        private void ResetId()
        {
            switch (curentMode)
            {
                case Mode.towers:
                case Mode.folders:
                    if (curentList < maxLists-1)
                    {
                        allowId.value = allowId.start = curentList * maxOnList;
                        allowId.end = ((curentList + 1) * maxOnList)-1;
                        
                    }
                    else
                    {
                        allowId.value = allowId.start = curentList * maxOnList;
                        allowId.end = (curentList * maxOnList + onLastList)-1 ;
                    }
                    
                    break;
            }
           
        }

        private void NextList(object sender, EventArgs args)
        {
            //Debug.Log("Next List");
            curentList++;
            if (curentList >= maxLists-1)
            {
                curentList = maxLists - 1;
                NextButton.Interactble = false;
                PrewButton.Interactble = true;
            }
            else
                PrewButton.Interactble = true;

            ChangeButtons();
        }
        private void PrewList(object sender, EventArgs args)
        {
            switch (curentMode)
            {
                case Mode.folders:
                    //Debug.Log("Previosly List");
                    curentList--;
                    if (curentList <= 0)
                    {
                        curentList = 0;
                        PrewButton.Interactble = false;
                        NextButton.Interactble = true;
                    }
                    else
                        NextButton.Interactble = true;
                    break;
                case Mode.towers:
                    //Debug.Log("Previosly List");
                    curentList--;
                    if (curentList < 0)
                    {
                        curentList = 0;
                        NextButton.Interactble = true;
                        PrewButton.Interactble = false;
                        curentMode = Mode.folders;
                        curentFolder = -1;
                        //TODO add method
                        LoadMode();
                        return;
                    }
                    else
                        PrewButton.Interactble = true;
                    break;
            }
            

            ChangeButtons();
        }
        private void OnPressButton(object sender, EventArgs args)
        {
            //Debug.Log("Entering in folder");
            int localButtonId = -1;
            for (int i = 0; i < funcButtons.Length; i++)
                if (sender as UIButton == funcButtons[i])
                {
                    localButtonId = i;
                    break;
                }
            if (localButtonId == 6)
                localButtonId = 5;
            switch (curentMode)
            {
                case Mode.folders:
                    
                    curentFolder = buttonsData[localButtonId];
                    curentMode = Mode.towers;
                    
                    //LOAd METHOD
                    LoadMode();
                    break;
                case Mode.towers:
                    BuildManager.ApplyToBuild(buttonsData[localButtonId],worldPoint);
                    break;
            }
           

        }
        private void OnClickButton(object sender, EventArgs args)
        {
            int localButtonId = -1;
            for (int i = 0; i < funcButtons.Length; i++)
                if (sender as UIButton == funcButtons[i])
                {
                    localButtonId = i;
                    break;
                }
            if (localButtonId == 6)
                localButtonId = 5;

            switch (curentMode)
            {
                case Mode.folders:
                    break;
                case Mode.towers:
                    ShowBlueprintInfo(buttonsData[localButtonId]);
                    break;
            }
        }

        
        private Sprite MorphSprite(PowerImageName sprName)
        {
            switch (sprName)
            {
                case PowerImageName.Heart:
                    return powerSrites[1];
                case PowerImageName.Fist:
                    return powerSrites[2];
                case PowerImageName.Bow:
                    return powerSrites[3];
                case PowerImageName.Lightning:
                    return powerSrites[4];
                case PowerImageName.Shield:
                    return powerSrites[5];
                case PowerImageName.Pickaxe:
                    return powerSrites[6];
                default: return powerSrites[0];
            }
        }

        /*private string MorphSingle(float value, int minNumbers = 2)
        {
            string buffer = value.ToString(); int delta = 0;
            foreach(var ch in buffer)
                if(ch == '.' || ch == ',')
                {
                    delta = 1;
                    break;
                }
            int length = minNumbers + delta;
            if (length > buffer.Length)
            {
                length = buffer.Length;
                char[] exitBuffer = new char[length]; int myI = 0;
                for (int i = 0; i < length; i++)
                {
                    exitBuffer[myI++] = buffer[i];
                }
                return exitBuffer.ArrayToString();
            }
            else if (buffer.Length / 10f >= 1f)
                return value.ToString("0");
            return buffer;
        }*/

        void ShowBlueprintInfo(int buildId)
        {
            var tower = BuildManager.Instance.buildList[buildId];
            int Next(ref int value)
            {
                value++;
                if (value == 8)
                    value = 0;
                return value;
            }
            mainSprt.sprite = tower.sprite;
            mainName.text = tower.Actor.name;
            goldValue.text = tower.buildPrice.ToString("0");
            materialValue.text = tower.buildMaterialPrice.ToString("0");
            buildValue.text = tower.buildTime.ToString("0");
            var powerInfo = (tower as IBuilding).GetUIData;
            int startIndex = 0, index;
            switch(powerInfo.Length)
            {
                case 3:
                    startIndex = 1;
                    break;
                case 1:
                    startIndex = 2;
                    break;
                case 7:
                    startIndex = 7;
                    break;
                default:
                    startIndex = 0;
                    break;

            }
            index = startIndex;
            int length = 8;
            if (length > powerInfo.Length)
                length = powerInfo.Length;
            //Powers
            for(int i =0;i<8;i++)
            {
                if (i < length)
                {
                    powerImgs[index].gameObject.SetActive(true);
                    powerValues[index].gameObject.SetActive(true);

                    powerImgs[index].sprite = MorphSprite(powerInfo[i].imageName);
                    powerImgs[index].color =  Maths.Utility.MorphColor(powerInfo[i].imageColor);
                    powerValues[index].text = Maths.Utility.MorphSingle(powerInfo[i].value);
                    Next(ref index);
                }
                else
                {
                    powerImgs[index].gameObject.SetActive(false);
                    powerValues[index].gameObject.SetActive(false);
                    Next(ref index);
                    continue;
                }
                /*if(powerSrites.Length > i && powerSrites[i]!=null)
                {
                    powerImgs[i].gameObject.SetActive(true);
                    powerImgs[i].sprite = powerSrites[i];

                    switch (i)
                    {
                        case 0: //hp
                            powerValues[i].text = tower.Actor.health.MaxValue.ToString("0");
                            break;
                        case 1: //dmg
                            powerValues[i].text = tower.Actor.AttackModule.Damage.ToString("0.0");
                            break;
                        case 2: //range
                            powerValues[i].text = tower.Actor.AttackModule.range.ToString("0.0");
                            break;
                        case 3: //speed
                            powerValues[i].text = tower.Actor.AttackModule.AttacksPerMinute.ToString("0");
                            break;
                        case 4: //armor
                            powerValues[i].text = tower.Actor.linearArmor.ToString("0.0");
                            break;
                    }
                }
                else
                {
                    powerImgs[i].gameObject.SetActive(false);
                    powerValues[i].text = "";
                }*/
                
            }
        }

        AvalibleSlots oldSlots;
        private void Start()
        {

            foreach (var brn in funcButtons)
            {
                brn.Interactble = true;
                brn.OnPressButtonEvent += OnPressButton;
                brn.OnClickButtonEvent += OnClickButton;
            }
            oldSlots = slots;
            //NextAndPrew
            NextButton.OnPressButtonEvent += NextList;
            PrewButton.OnPressButtonEvent += PrewList;
            
            //Next init
            ReinitFolders();
            LoadMode();


            Control.Selectable.Instance.GUIs.Add(this);
            gameObject.SetActive(false);
           

        }
        [SerializeField]
        TextMeshProUGUI buildersCount;
        int oldValue = 0;
        private void Update()
        {
           Vector3 pos = Camera.main.WorldToScreenPoint(worldPoint);
            transform.position = pos;
            if(oldValue!=BuildManager.FreeBuilders)
            {
                oldValue = BuildManager.FreeBuilders;
                buildersCount.text = $"{oldValue} / {BuildManager.Instance.builders.Count}";
            }
        }
        /// <summary>
        /// MAIN CALL
        /// </summary>
        public void Call(Vector2 pos)
        {
            gameObject.SetActive(true);
            worldPoint = pos;
            transform.position = Camera.main.WorldToScreenPoint(worldPoint);
            Control.Selectable.Instance.UnSelectable = true;
        }

        /// <summary>
        /// MAIN
        /// </summary>
        public void LoadMode()
        {
            var tmp = transform.GetChild(0);
            switch (curentMode)
            {
                case Mode.folders:

                    if (oldFolderList == -1)
                        curentList = 0;
                    else
                        curentList = oldFolderList;
                    maxLists = (folders.Count / maxOnList);
                    if (maxLists < folders.Count / (float)maxOnList)
                        maxLists++;
                    
                    //if (folders.Count / 6f >= maxLists)
                        onLastList = (folders.Count % maxOnList);

                    //BlueprintInfo

                    //tmp = transform.GetChild(0);
                    tmp.GetChild(0).gameObject.SetActive(false);
                    tmp.GetChild(1).gameObject.SetActive(false);

                    ChangeButtons();
                    break;

                case Mode.towers:
                    oldFolderList = curentList;
                    curentList = 0;
                    maxLists = folders[curentFolder].items.Count / maxOnList;
                    if (maxLists < folders[curentFolder].items.Count / (float)maxOnList)
                        maxLists++;
                    onLastList = (folders[curentFolder].items.Count % maxOnList);

                    //BlueprintInfo

                    //tmp = transform.GetChild(0);
                    tmp.GetChild(0).gameObject.SetActive(true);
                    tmp.GetChild(1).gameObject.SetActive(true);

                    //PrewButton.Interactble = true;
                    ChangeButtons();
                    break;
            }

            NextButton.Interactble = true;
            PrewButton.Interactble = true;
            NextButton.changeImg = PrewButton.changeImg = true;
            if (curentList >= maxLists - 1)
                NextButton.Interactble = false;
            if (curentList == 0 && curentMode == Mode.folders)
                PrewButton.Interactble = false;
            
        }
        
        public void ChangeButtons()
        {
            foreach (var line in Lines)
                line.gameObject.SetActive(false);
            foreach (var btn in buttons)
                btn.gameObject.SetActive(false);
            buttonsData.Clear(); 
            int i = 0; int btnId = 0; ResetId();

            int slotsCount = allowId.end-allowId.start;
            if (allowId.start == 0)
                slotsCount++;
            if (slotsCount > maxOnList)
                slotsCount = maxOnList;
            else if (slotsCount < 2)
                slotsCount = 2;
            slots = (AvalibleSlots)slotsCount;

            switch (slots)
            {
                case AvalibleSlots.x2:
                    //Buttons
                    btnId = 1;
                    ButtonUpdate(btnId);
                    btnId = 4;
                    ButtonUpdate(btnId);
                    break;
                case AvalibleSlots.x3:
                    //Lines
                    Lines[i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 90f);
                    //Buttons
                    ButtonUpdate(btnId);
                    btnId = 2;
                    ButtonUpdate(btnId);
                    btnId = 4;
                    ButtonUpdate(btnId);
                    break;
                case AvalibleSlots.x4:
                    //Lines
                    Lines[i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 90f);
                    i = 2;
                    Lines[i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 270f);
                    //Buttons
                    ButtonUpdate(btnId);
                    btnId = 2;
                    ButtonUpdate(btnId);
                    btnId = 3;
                    ButtonUpdate(btnId);
                    btnId = 5; //Btn 5
                    ButtonUpdate(btnId);
                    break;
                case AvalibleSlots.x5:
                    //Lines
                    Lines[i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 90f-22.5f);
                    Lines[++i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 90f+22.5f);
                    Lines[++i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 270f);
                    //Buttons
                    ButtonUpdate(btnId);
                    ButtonUpdate(++btnId);
                    ButtonUpdate(++btnId);
                    ButtonUpdate(++btnId);
                    btnId = 5;
                    ButtonUpdate(btnId);
                    break;
                case AvalibleSlots.x6:
                    //Lines
                    Lines[i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 90f - 22.5f);
                    Lines[++i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 90f + 22.5f);
                    Lines[++i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 180f + 90f - 22.5f);
                    Lines[++i].gameObject.SetActive(true);
                    Lines[i].transform.rotation = Quaternion.Euler(0, 0, 180f + 90f + 22.5f);
                    //Buttons
                    ButtonUpdate(btnId);
                    ButtonUpdate(++btnId);
                    ButtonUpdate(++btnId);
                    ButtonUpdate(++btnId);
                    ButtonUpdate(++btnId);
                    ButtonUpdate(++btnId);
                    break;
                    
            }
            void ButtonUpdate(int buttonId)
            {
                int realId = NextId();
                buttons[buttonId].gameObject.SetActive(true);
                //buttonsData.Add(buttonId, realId);
                switch (curentMode)
                {
                    case Mode.folders:
                        buttonsData.Add(buttonId, realId);
                        buttons[buttonId].Find("Box").GetComponentInChildren<Image>(false).sprite = folderSpr;
                        buttons[buttonId].Find("Box").GetComponentInChildren<TextMeshProUGUI>().text = folders[realId].name;                    
                        break;
                    case Mode.towers:
                        buttonsData.Add(buttonId, folders[curentFolder].items[realId]);
                        buttons[buttonId].Find("Box").GetComponentInChildren<Image>(false).sprite = BuildManager.Instance.buildList[buttonsData[buttonId]].sprite;
                        buttons[buttonId].Find("Box").GetComponentInChildren<TextMeshProUGUI>().text = BuildManager.Instance.buildList[buttonsData[buttonId]].Actor.name;
                        break;
                }
            }
        }

        public void ReinitFolders()
        {
            var list = BuildManager.Instance.buildList;
            if (folders == null)
                folders = new List<Folder>(3);
            else
                folders.Clear();
            for(int i =0;i<list.Count;i++)
            {
                if(!folders.Contains(new Folder(Tools.GetFirstName(list[i].Path))))
                {
                    folders.Add(new Folder(Tools.GetFirstName(list[i].Path), i));
                }
                else
                {
                    folders.Find((x) => x.name.Equals(Tools.GetFirstName(list[i].Path))).items.Add(i);
                }
            }
        }

        void IGUI.Close()
        {
            gameObject.SetActive(false);
        }

        struct Tools
        {
            public static string GetFirstName(string name)
            {
                int i = 0;
                while (name[i] != '/')
                    i++;
                return name.Substring(0, i);
            }
            public static string GetSecondName(string name)
            {
                int i = 0;
                while (name[i] != '/')
                    i++;
                return name.Substring(++i, name.Length-i);
            }
        }

        [System.Serializable]
        struct Folder
        {
            public string name;
            public List<int> items;

            public static bool operator==(Folder left, Folder right)
            {
                if (string.Equals(left.name,right.name))
                    return true;
                else
                    return false;
            }
            public static bool operator !=(Folder left, Folder right)
            {
                if (string.Equals(left.name, right.name))
                    return false;
                else
                    return true;
            }

            public Folder(string folderName, params int[] items)
            {
                name = folderName;
                this.items = new List<int>(items);
            }

            public override bool Equals(object obj)
            {
                if (obj is Folder)
                    if (string.Equals(name, ((Folder)obj).name))
                        return true;
                return false;
            }
        }


    }
}
